import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import Dashboard from './components/Dashboard'
import List from './components/List'
import Survey from './components/Canvass/Survey'
import Help from './components/Help'
import Questions from './components/Canvass/Questions'
import Confirm from './components/Canvass/Confirm'
import Success from './components/Canvass/Success'
import CompletedTasks from './components/CompletedTasks'

const App = () => {
    return(
        <BrowserRouter>
            <div>
                <NavBar />
                <Route exact path="/" component={Dashboard}/>
                <Route path="/Canvass/Survey" component={Survey}/>
                <Route path="/List" component={List}/>
                <Route path="/Help" component={Help}/>
                <Route path="/Questions" component={Questions}/>
                <Route path="/Confirm" component={Confirm}/> 
                <Route path='/Success' component={Success}/>
                <Route path='/CompletedTasks' component={CompletedTasks}/>
            </div>
        </BrowserRouter>
    )
}

export default App