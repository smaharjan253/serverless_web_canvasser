import React from 'react'
import { Button, Typography } from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from './component-styles/NavBarStyle'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';

const CompletedTasks = () => {
    const classes = useStyles();

    return(
        <div className="container">
           
            <Typography color="primary" style={{marginLeft:"150px", marginTop:"50px"}}>
                My Completed Task Log
            </Typography>
            
        </div>
    )
}
export default CompletedTasks