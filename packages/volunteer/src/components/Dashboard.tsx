import React from 'react'
import { Button, Box } from '@material-ui/core'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import { useState } from 'react'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import DashboardCommonComponent from './dashboardCommonComponent'
import constant from './constant'
import { useHistory } from 'react-router-dom'
import jsonData from "@canvasser/components/src/stack.json"
import useStyles from './component-styles/NavBarStyle';
const Dashboard = () => {
    const history = useHistory()
    const classes = useStyles();
    const [data, setData] = useState({records:[],state:false});
    
    const requestData = ()=>{
        const url = jsonData.ServiceEndpoint + '/getRecordsByVolunteerId'
        console.log(url)
        // const url = ''
        const callback = (dataLoad:any)=>{
            setData({records:dataLoad,state:true})
        }
        const VolunteerId = 'VolunteerId'
        fetch(url, {
            method: 'POST',
            headers: { 'Accept': 'application/json',
            'x-api-key': jsonData.apikey,
           },
            body: JSON.stringify({
                volunteerId: VolunteerId,
            }),
        })
            .then(function (response) {
                return response.json()
            })
            .then(function (myjson) {
                console.log(myjson);
                
                callback(myjson)
                
            })
            .catch(error => console.error('Error:', error));;
    }

    const editCallback = ()=>{
        history.push('/AddVolunteer')
    }
    const deleteCallBack = ()=>{

    }
    if(data.state === false){
        requestData()
    }
    
    return(
        <div className="container">
            <div className={classes.wholePanel}>
                <div className={classes.childComponentContainer}>
                    <DashboardCommonComponent editCallback={editCallback} deleteCallback={deleteCallBack} data={data}/>
                </div>
            </div>
            <Box className={classes.inComponentButton}>
                <Button 
                    component={Link} 
                    to="/CompletedTasks"
                >
                    Completed Task Log
                    <AddBoxOutlinedIcon />
                </Button>
            </Box>

         </div>   
    )
}

export default Dashboard
