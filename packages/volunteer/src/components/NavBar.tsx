import React from 'react'
import { Link } from 'react-router-dom'
import useStyles from './component-styles/NavBarStyle'
import buttonProps from './component-styles/ButtonData'
import { AppBar, Toolbar, Typography, useTheme, Hidden,
     IconButton, CssBaseline, List, Drawer, Divider, ListItem, Button } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu'
import DashboardIcon from '@material-ui/icons/Dashboard'
import ForumIcon from '@material-ui/icons/Forum';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import HelpIcon from '@material-ui/icons/Help'
import PersonOutlineIcon from '@material-ui/icons/PersonOutline'

interface NavBarProps {
    container?: Element;
  }

const NavBar: React.FC<NavBarProps> = props => {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const userName = "Lord Saubhagya";
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const date = new Date();
  const monthIndex = date.getMonth();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  // const handleUserName = () => {
  //   setUserName();
  // }
  

  const drawer = (
    <div>

        <Divider />
        
                {buttonProps.map(button => ( 
                  <List key={button.id} className={classes.navButton}>
                    <ListItem 
                      button
                      
                      component={Link} 
                      to={button.path}>
                      {
                        button.id === 1 ? <DashboardIcon/> :
                        button.id === 2 ? <ForumIcon/> : <AssignmentOutlinedIcon/>
                      }
                    </ListItem>
                  </List>
                ))}
                
        

            <List className={classes.help}>
                <ListItem button component={Link} to="/help">
                    <HelpIcon/>
                </ListItem>
            </List>
        
    </div>
  );

  return (
    <div>
      <CssBaseline />
      
      <AppBar position="fixed" className={classes.appBar} elevation={0} >
        <Toolbar>
          <IconButton
            color="inherit"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>

          <Button component={Link} to="/">
            <Typography className={classes.title} variant="h4" noWrap>
              Web Canvasser
            </Typography>
          </Button>

          <div className={classes.logout}>
            <Hidden smDown>
              <PersonOutlineIcon color="primary" fontSize="small" className="welcomeTextLogo"/>
              <Typography className="welcomeText" variant="subtitle1" color="primary">
                Welcome {userName} | {date.getDate()} {monthNames[monthIndex]} {date.getFullYear()}
              </Typography>
            </Hidden>
        
            <Button component={Link} to="/">
                <ExitToAppIcon/>
            </Button>
          </div>

        </Toolbar> 
      </AppBar>
      
      <nav className={classes.drawer}>
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}

export default NavBar