const questionData = [
    {
        id: 'Q1',
        question: 'Who are you going to vote for, bro?',
        type: 'text'
    },
    {
        id: 'Q2',
        question: 'Who are you NOT going to vote for, fam?',
        type: 'radio'
    },
    {
        id: 'Q3',
        question: 'Who do you think is going to win the vote in the end, mate?',
        type: 'select'
    },
]

export default questionData