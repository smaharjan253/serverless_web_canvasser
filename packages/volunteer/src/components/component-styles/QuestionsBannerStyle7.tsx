import React from 'react'
import Question1Done from './Images/0QuestionDone.svg';
import Question2Done from './Images/0QuestionDone.svg';
import Question3Done from './Images/0QuestionDone.svg';
import Question4Done from './Images/0QuestionDone.svg';
import Question5Done from './Images/0QuestionDone.svg';
import Question6Done from './Images/0QuestionDone.svg';
import OnQuestion7 from './Images/1-7OnQuestion7.svg';

const QuestionsBannerStyle7 = () => {

    return(
            <div className="full-image-question">
        
                <div className="row">

                    <div className="column">
                    <img src={Question1Done} alt="group1"/>
                    </div>
                    
                    <div className="column">
                    <img src={Question2Done} alt="group2"/>
                    </div>

                    <div className="column">
                    <img src={Question3Done} alt="group3"/>
                    </div>

                    <div className="column">
                    <img src={Question4Done} alt="group4"/>
                    </div>

                    <div className="column">
                    <img src={Question5Done} alt="group5"/>
                    </div>

                    <div className="column">
                    <img src={Question6Done} alt="group6"/>
                    </div>

                    <div className="column">
                    <img src={OnQuestion7} alt="group7"/>
                    </div>
                </div>
            </div>
    )
}

export default QuestionsBannerStyle7