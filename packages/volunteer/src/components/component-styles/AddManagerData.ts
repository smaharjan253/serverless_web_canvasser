const addManagerProps = [
    {
        id: "preferred-location",
        label: 'Preferred Location'
    },
    {
        id: "full-name",
        label: 'Full Name'
    },
    {
        id: "age",
        label: 'Age'
    },
    {
        id: "contact-no",
        label: 'Contact No.'
    },
    {
        id: "e-mail",
        label: 'E-Mail'
    },
    {
        id: "password",
        label: 'Password'
    },
    {
        id: "confirm-password",
        label: 'Confirm Password'
    }
]

export default addManagerProps