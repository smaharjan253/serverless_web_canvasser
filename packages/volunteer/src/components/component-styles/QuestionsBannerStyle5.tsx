import React from 'react'
import Question1Done from './Images/0QuestionDone.svg';
import Question2Done from './Images/0QuestionDone.svg';
import Question3Done from './Images/0QuestionDone.svg';
import Question4Done from './Images/0QuestionDone.svg';
import OnQuestion5 from './Images/1-5OnQuestion5.svg';
import Question6 from './Images/6NotQuestion6.svg';
import Question7 from './Images/7NotQuestion7.svg';

const QuestionsBannerStyle5 = () => {

    return(
            <div className="full-image-question">
        
                <div className="row">

                    <div className="column">
                    <img src={Question1Done} alt="group1"/>
                    </div>
                    
                    <div className="column">
                    <img src={Question2Done} alt="group2"/>
                    </div>

                    <div className="column">
                    <img src={Question3Done} alt="group3"/>
                    </div>

                    <div className="column">
                    <img src={Question4Done} alt="group4"/>
                    </div>

                    <div className="column">
                    <img src={OnQuestion5} alt="group5"/>
                    </div>

                    <div className="column">
                    <img src={Question6} alt="group6"/>
                    </div>

                    <div className="column">
                    <img src={Question7} alt="group7"/>
                    </div>
                </div>
            </div>
    )
}

export default QuestionsBannerStyle5