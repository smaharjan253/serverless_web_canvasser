import React from 'react'
import OnQuestion1 from './Images/1-1OnQuestion1.svg';
import Question2 from './Images/2NotQuestion2.svg';
import Question3 from './Images/3NotQuestion3.svg';
import Question4 from './Images/4NotQuestion4.svg';
import Question5 from './Images/5NotQuestion5.svg';
import Question6 from './Images/6NotQuestion6.svg';
import Question7 from './Images/7NotQuestion7.svg';

const QuestionsBannerStyle1 = () => {

    return(
            <div className="full-image-question">
        
                <div className="row">

                    <div className="column">
                    <img src={OnQuestion1} alt="group1"/>
                    </div>
                    
                    <div className="column">
                    <img src={Question2} alt="group2"/>
                    </div>

                    <div className="column">
                    <img src={Question3} alt="group3"/>
                    </div>

                    <div className="column">
                    <img src={Question4} alt="group4"/>
                    </div>

                    <div className="column">
                    <img src={Question5} alt="group5"/>
                    </div>

                    <div className="column">
                    <img src={Question6} alt="group6"/>
                    </div>

                    <div className="column">
                    <img src={Question7} alt="group7"/>
                    </div>
                </div>
            </div>
    )
}

export default QuestionsBannerStyle1