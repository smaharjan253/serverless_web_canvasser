import React from 'react';
import clsx from 'clsx';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles';
import { Button, Container, CssBaseline, Divider, Box, Drawer } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import addButton from './Add New Button.svg'
import moreButton from './more button.svg'
import { Link } from 'react-router-dom'
interface Data {
    calories: number;
    carbs: number;
    fat: number;
    name: string;
    protein: number;
    action: string;
}

function createData(
    name: string,
    calories: number,
    fat: number,
    carbs: number,
    protein: number,
    action: string
) {
    return { name, calories, fat, carbs, protein, action };
}
// const tableTitle = 'Manage Users'
const rows = [
    createData('Cupcake', 305, 3.7, 67, 4.3, ''),
    createData('Donut', 452, 25.0, 51, 4.9, ''),
    createData('Eclair', 262, 16.0, 24, 6.0, ''),
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0, ''),
    createData('Gingerbread', 356, 16.0, 49, 3.9, ''),
    createData('Honeycomb', 408, 3.2, 87, 6.5, ''),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3, ''),
    createData('Jelly Bean', 375, 0.0, 94, 0.0, ''),
    createData('KitKat', 518, 26.0, 65, 7.0, ''),
    createData('Lollipop', 392, 0.2, 98, 0.0, ''),
    createData('Marshmallow', 318, 0, 81, 2.0, ''),
    createData('Nougat', 360, 19.0, 9, 37.0, ''),
    createData('Oreo', 437, 18.0, 63, 4.0, ''),
];

const rows2 = [
    ['Cupcake', 305, '@'],
    ['Donut', 452, '@'],
    ['Eclair', 262, '@'],
    ['Gingerbread', 356, '@'],
    ['Honeycomb', 408, '@'],
    ['Ice cream sandwich', 237, '@'],
    ['Jelly Bean', 375, '@'],
    ['KitKat', 518, '@'],
    ['Lollipop', 392, '@'],
    ['Marshmallow', 318, '@'],
    ['Nougat', 360, '@'],
    ['Oreo', 437, '@']
]



function sortByKey(array: any, order: string, orderBy: number) {
    // console.log(order)
    // console.log(orderBy)
    array.sort((a: any, b: any) => {
        const bigger = a[orderBy] > b[orderBy];
        const result = order === 'asc' ? bigger ? 1 : -1 : bigger ? -1 : 1;
        return result
    });
    // console.log(array)
    return array
}

type Order = 'asc' | 'desc';



interface HeadCell {
    disablePadding: boolean;
    id: number;
    label: string;
    numeric: boolean;
    needsort: boolean;
}



interface EnhancedTableProps {
    classes: any;
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: number) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
    order: Order;
    orderBy: number;
    rowCount: number;
    header: Array<any>;
    // tableTitle:string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, header } = props;
    const createSortHandler = (property: number) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };
    const createEmptyHandler = (property: number) => (event: React.MouseEvent<unknown>) => {
        // onRequestSort(event, property);
    };

    return (
        <TableHead className={classes.tableHead}>
            <TableRow className={classes.tableHead}>
                {/* <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell> */}
                {header.map((headCell, index) => (
                    <TableCell
                        key={headCell.id}
                        align={index == 0 ? 'left' : index == header.length - 1 ? 'right' : 'center'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                        className={classes.tableFirstRow}
                    >
                        <TableSortLabel
                            className={index == 0 ? classes.tableHeadFirstFont : classes.tableHeadFont}
                            hideSortIcon={headCell.needsort ? false : true}
                            //   active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={headCell.needsort ? createSortHandler(headCell.id) : createEmptyHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

const useToolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            height: '10%',
            minHeight: 30,
            paddingLeft: 0,
            paddingRight: 0,
            //   height:'50%'
        },
        inComponentButton: {
            '& > *': {
              margin: theme.spacing(1),
              flex: '0.8',
              float: 'right',
              fontFamily: "PT Sans",
              fontWeight: "bold"
            },
          },
        highlight:
            theme.palette.type === 'light'
                ? {
                    color: theme.palette.secondary.main,
                    backgroundColor: lighten(theme.palette.secondary.light, 0.85),
                }
                : {
                    color: theme.palette.text.primary,
                    backgroundColor: theme.palette.secondary.dark,
                },
        title: {
            flex: '8',
            fontSize: 20,
            color: '#0A437C',
            fontFamily: 'Segoe UI',
            minHeight: 20
        },
        addButtonTitle: {
            flex: '2',
            textAlign: 'right',
            fontWeight: 'bold',

        },
        addButton: {
            flex: '0.8',
            padding: 0,
        }
    }),
);
// Interface starts here
interface EnhancedTableToolbarProps {
    numSelected: number;
    addOrMore: boolean;
    tableTitle:string;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected, addOrMore,tableTitle } = props;
    const icon = addOrMore ? addButton : moreButton;
    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <Typography className={classes.title} >
                {tableTitle}
            </Typography>

            {/* Component button */}
            <Box className={classes.inComponentButton}>
                <Button 
                    component={Link} 
                    to="/CompletedTasks"
                >
                    More
                    <AddBoxOutlinedIcon />
                </Button>
            </Box>




        </Toolbar>
    );
};


interface EnhancedTableAllProps {
    data: Array<any>;
    headers: Array<string>
    editCallback: any;
    deleteCallback: (id: string) => void;
    pageSize: number;
    minH: number;
    actionShow: boolean;
    addOrMore: boolean;
    tableTitle:string;
}
export default function SmallSortedLIstComponent(props: EnhancedTableAllProps) {

    //added
    const { data, headers, editCallback, deleteCallback, pageSize, minH, actionShow, addOrMore, tableTitle } = props


    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<number>(1);
    const [selected, setSelected] = React.useState<string[]>([]);
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(true);
    const [rowsPerPage, setRowsPerPage] = React.useState(pageSize);


    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                width: '100%',
            },
            paper: {
                width: '100%',
                marginBottom: theme.spacing(2),
            },
            table: {
                marginTop: theme.spacing(2),
                minWidth: 400,
                // float:'left'
            },
            visuallyHidden: {
                border: 0,
                clip: 'rect(0 0 0 0)',
                height: 1,
                margin: -1,
                overflow: 'hidden',
                padding: 0,
                position: 'absolute',
                top: 20,
                width: 1,
            },
            tableContainer: {
                paddingLeft: theme.spacing(1),
                paddingRight: theme.spacing(1),
                maxHeight: minH,
                // height:'100%',
                // display:'table',
                width: '100%'
            },
            tableFirstRow: {
                height: 10,
                borderBottomWidth: 0,
                borderBottom: 'none',
                minHeight: 10,
                // elevation:0
            },
            tableHead: {
                backgroundColor: '#F5F6FA',
                // height: 10,
                // minHeight: 10,
            },
            tableHeadFont: {
                fontFamily: 'HELVETICA',
                fontWeight: 'bold',
                color: '#7B7C81',
                fontSize: 10,
                paddingRight: 0,
            },
            tableHeadFirstFont: {
                fontFamily: 'HELVETICA',
                fontWeight: 'bold',
                color: '#7B7C81',
                fontSize: 10,
                marginLeft: theme.spacing(2),
            },
            item: {
                color: '#4A4A4A',
                fontFamily: 'Segoe UI',
                fontSize: '12px',
                width: 'fit-content',
                minWidth: 10,
                paddingTop: '20px',
            },
            itemOne: {
                color: '#4A4A4A',
                fontFamily: 'Segoe UI',
                fontSize: '12px',
                width: 'fit-content',
                minWidth: 10,
                paddingLeft: theme.spacing(2),
                paddingTop: theme.spacing(2),
                // marginBottom:theme.spacing(0),
            },
            recordRow: {
                paddingBottom: 0
            },

        }),
    );
    const classes = useStyles();

    const handleRequestSort = (event: React.MouseEvent<unknown>, property: number) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.name);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected: string[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleEdit = (event: React.MouseEvent<unknown>, id: string) => {
        //jump to edit page
        const dataForEdit = data.filter((item) => {
            return item[0] === id
        });
        editCallback(dataForEdit);
    }

    const handleDelete = (event: React.MouseEvent<unknown>, id: string) => {
        //sendrequest to delete data and pull new data
        deleteCallback(id)
    }

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDense(event.target.checked);
    };



    const isSelected = (name: string) => selected.indexOf(name) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div className={classes.root}>
            {/* <Paper className={classes.paper}> */}
            <EnhancedTableToolbar numSelected={0} addOrMore={addOrMore} tableTitle={tableTitle}/>
            <TableContainer className={classes.tableContainer}>
                <Table
                    // stickyHeader
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size={dense ? 'small' : 'medium'}
                    aria-label="enhanced table"
                >
                    <EnhancedTableHead
                        classes={classes}
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                        header={headers}
                    />
                    <TableBody>
                        {sortByKey(data, order, orderBy)
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row: any, index: any) => {
                                const isItemSelected = isSelected(row.name);
                                const labelId = `enhanced-table-checkbox-${index}`;
                                // console.log(row[0])
                                return (
                                    <TableRow
                                        hover
                                        onClick={event => handleClick(event, row[0])}
                                        //   role="checkbox"
                                        //   aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row[0]}
                                        className={classes.recordRow}
                                    //   selected={isItemSelected}
                                    >
                                        <TableCell className={classes.itemOne} align="left" component="th" id={labelId} scope="row" padding="none" >
                                            {row[0]}
                                        </TableCell>
                                        {row.map((item: any, index: number) => {
                                            if (index == 0) return
                                            if (!actionShow && index == row.length - 1) return (<TableCell className={classes.item} key={item + index} align="right">{row[index]}</TableCell>)
                                            return (<TableCell className={classes.item} key={item + index} align="center">{row[index]}</TableCell>)
                                        })}
                                        {(actionShow) ?
                                            <TableCell align="right" className={classes.item}>
                                                {/* <Button component={Link}
                                                    to="/AddVolunteer"> */}
                                                    <EditIcon

                                                        onClick={event => handleEdit(event, row[0])}
                                                    />
                                                {/* </Button> */}
                                                <DeleteIcon
                                                    onClick={event => handleDelete(event, row[0])}
                                                />
                                            </TableCell> : null}


                                    </TableRow>
                                );
                            })}
                        {emptyRows > 0 && (
                            <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {/* <TablePagination
                rowsPerPageOptions={[30]}
                component="div"
                count={rows.length}
                rowsPerPage={30}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}

            /> */}
            {/* </Paper> */}
        </div>
    );
}
