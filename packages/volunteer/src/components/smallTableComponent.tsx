import React from "react"
import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography } from "@material-ui/core"
import { useHistory } from 'react-router-dom';
import SmallSortedLIstComponent from './smallSortedLIstComponent';
interface CommonTableComponentProps {
    editCallback: any;
    deleteCallback: (id:string)=>void ;
    pageSize:number;
    minH:number;
    actionShow:boolean;
    data:any[];
    headCell:any[];
    addOrMore:boolean;
    tableTitle:string;
  }
const SmallTableComponent: React.FC<CommonTableComponentProps> = (props) => {
    const {editCallback,deleteCallback,pageSize,minH,actionShow,data,headCell,addOrMore,tableTitle} = props
    // const [data, setData] = useState({ title: 'account info', dataList: [] });
    const history = useHistory()
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            marginTop:theme.spacing(2),
            backgroundColor: '#ffffff',
            height: '90%',
            width: '100%',

        },
        title: {
            paddingTop: theme.spacing(4),
            paddingLeft: theme.spacing(4),
            width: '100%',
            height: '10%',
        },
        titleText: {
            paddingLeft: theme.spacing(2),
            color: '#778899',
            fontSize: 20,
        },
        tableContainer: {
            width: '100%',
            height: '100%',
            paddingTop: theme.spacing(1),
            paddingLeft: theme.spacing(1),
            paddingRight: theme.spacing(1),
            // backgroundColor: '#fff',
        },
        tableLayout: {
            // paddingLeft:theme.spacing(4),
            // borderRadius: 10,
            // padding: theme.spacing(4),
            width: '100%',
            height: '100%',
            // maxHeight: 600,
            backgroundColor: '#fff',
        },
        table: {
            minWidth: 650,
            maxHeight: minH
        },
        tableTitle: {
            // color:'#00FFFF'
        }
    }));

    const row3 = [
        ['Cupcake', 305],
        ['Donut', 452],
        ['Eclair', 262],
        ['Gingerbread', 356],
        ['Honeycomb', 408],
        ['Ice cream sandwich', 237],
        ['Jelly Bean', 375],
        ['KitKat', 518],
        ['Lollipop', 392],
        ['Marshmallow', 318],
        ['Nougat', 360],
        ['Oreo', 437]
    ]
    const headCells: any = [
        { id: 0, numeric: false, disablePadding: true, label: 'USERNAME', needsort: true },
        { id: 1, numeric: true, disablePadding: false, label: 'SURBURB', needsort: false },
        { id: 2, numeric: true, disablePadding: false, label: 'ACTION', needsort: false },
    ];

    const handleEdit = (dataForEdit: Array<any>) => {
        editCallback(dataForEdit)
    }
    const handleDelete = (dataForDelete: string) => {
        deleteCallback(dataForDelete)
    }
    const columns = ['dessert', 'calories', 'fat', 'carbs', 'protein']
    const classes = useStyles();

    return (
        <div className={classes.wholePanel}>
            <CssBaseline />

            <Container className={classes.tableContainer}>
                <div className={classes.tableLayout}>

                    <SmallSortedLIstComponent data={data} headers={headCell} editCallback ={handleEdit} 
                    deleteCallback={handleDelete} pageSize={pageSize} minH={minH} actionShow={actionShow}
                    addOrMore = {addOrMore} tableTitle={tableTitle}/>
                </div>
            </Container>
        </div>
    )
}

export default SmallTableComponent
