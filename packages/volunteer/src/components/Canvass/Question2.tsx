import React from 'react'
import { Box, Button, Typography, Hidden, RadioGroup, FormControlLabel, Radio, FormControl } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle2 from '../component-styles/QuestionsBannerStyle2';

const Question2 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className='questionstyle-2'>
            <Hidden smDown>
                <QuestionBannerStyle2/>
            </Hidden>
            <FormControl component="fieldset" className={classes.formControl}>
                <Typography variant="h4" color="primary">Is the voter available during the visit?</Typography>
                <br/>
                <RadioGroup value={props.value} onChange={props.handleChange}>
                    <FormControlLabel value="yes" control={<Radio />} label="Yes" />
                    <FormControlLabel value="no" control={<Radio />} label="No" />
                </RadioGroup>
                <br/><br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </FormControl>
        </Box>
    )
}

export default Question2