import React, { useState } from 'react'
import { Box, Button } from '@material-ui/core'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import useStyles from '../component-styles/NavBarStyle';
import { Link } from 'react-router-dom'

const Success = () => {
    return(
        <div className="container">
            <h1>Thank you! Your Survey has been Successfully Submitted!</h1>
        </div>
    )
}

export default Success