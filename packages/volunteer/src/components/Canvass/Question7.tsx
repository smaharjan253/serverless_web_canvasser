import React from 'react'
import { Box, Button, Hidden, Typography, TextField } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle7 from '../component-styles/QuestionsBannerStyle7';

const Question7 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className="questionstyle-2">
            <Hidden smDown>
                <QuestionBannerStyle7/>
            </Hidden>
            <form>
            <Typography variant="h4" color="primary">What's your policy preference?</Typography>
                <br/><br/>
                <br/>
                <TextField
                    onChange={props.handleChange}
                    variant="outlined" 
                    className="preferenceInput" 
                    placeholder="Preference"
                />
                <br/><br/>
                <br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </form>
        </Box>
    )
}

export default Question7