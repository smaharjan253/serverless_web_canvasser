import React from 'react'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import useStyles from '../component-styles/NavBarStyle';
import { Box, TextField, FormControl, RadioGroup, FormControlLabel, Radio, Select } from '@material-ui/core';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import questionData from '../component-styles/QuestionData'

const Survey = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }

    return(
        <div>
            <h1>Survey Questions</h1>
            {questionData.map(qData => (
                qData.type === 'text' ? 
                    <div>
                    <FormControl className={classes.formControl}>
                        <h3>{qData.question}</h3>
                        <TextField variant="outlined"/>
                    </FormControl> 
                    <br/>
                    </div>
                :
                qData.type === 'radio' ?
                <div>
                <FormControl component="fieldset" className={classes.formControl}>
                    <h3>{qData.question}</h3>
                        <RadioGroup aria-label="gender" name="gender1" value={props.value} onChange={props.handleChange}>
                            <FormControlLabel value="female" control={<Radio />} label="Your Father" />
                            <FormControlLabel value="male" control={<Radio />} label="Your Mother" />
                            <FormControlLabel value="other" control={<Radio />} label="Your Other Family Members" />
                        </RadioGroup>
                </FormControl> 
                <br/>
                </div>
                : 
                <div>
                <FormControl variant="outlined" className={classes.formControl}>
                    <h3>{qData.question}</h3>
                    <Select
                        native
                        value={props.value}
                        onChange={props.handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'outlined-age-native-simple',
                        }}
                    >
                  <option value="" />
                  <option value={10}>Papa</option>
                  <option value={20}>Mama</option>
                  <option value={30}>Dada</option>
                </Select>

              </FormControl>
              <br/>
              <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Confirmation
                    </Button>
                </div>
              </div>
            ))}
        </div>
    )
}

export default Survey