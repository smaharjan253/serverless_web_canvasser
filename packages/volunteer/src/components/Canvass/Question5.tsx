import React from 'react'
import { Box, Button, Hidden, Typography, Select, MenuItem } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle5 from '../component-styles/QuestionsBannerStyle5';

const Question5 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className="questionstyle-2">
            <Hidden smDown>
                <QuestionBannerStyle5/>
            </Hidden>
            <form>
                <Typography variant="h4" color="primary">
                    Age Group
                </Typography>
                <br/><br/>
                <br/>
                <Select className="selectSuburb">
                    <MenuItem>18-24</MenuItem>
                    <MenuItem>24-30</MenuItem>
                    <MenuItem>30-40</MenuItem>
                    <MenuItem>40-50</MenuItem>
                    <MenuItem>50-60</MenuItem>
                    <MenuItem>60-70</MenuItem>
                    <MenuItem>70-80</MenuItem>
                    <MenuItem>80+</MenuItem>
                </Select>
                <br/><br/>
                <br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </form>
        </Box>
    )
}

export default Question5