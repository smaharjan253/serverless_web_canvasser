import React from 'react'
import { Box, Button, Hidden, FormControl, RadioGroup, Typography, FormControlLabel, Radio } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle3 from '../component-styles/QuestionsBannerStyle3';

const Question3 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className="questionstyle-2">
            <Hidden smDown>
                <QuestionBannerStyle3/>
            </Hidden>
            <FormControl component="fieldset" className={classes.formControl}>
                <Typography variant="h4" color="primary">
                    Gender
                </Typography>
                <br/>
                <RadioGroup aria-label="gender" name="gender1" value={props.value} onChange={props.handleChange}>
                    <FormControlLabel value="female" control={<Radio />} label="Female" />
                    <FormControlLabel value="male" control={<Radio />} label="Male" />
                    <FormControlLabel value="other" control={<Radio />} label="Other/Unspecified" />
                </RadioGroup>
                <br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </FormControl>
        </Box>
       
    )
}

export default Question3