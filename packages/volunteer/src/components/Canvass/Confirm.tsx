import React, { useState } from 'react'
import { Box, Button } from '@material-ui/core'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import useStyles from '../component-styles/NavBarStyle';
import { Link } from 'react-router-dom'

const Confirm = (props: any) => {
    const classes = useStyles();

    return(
        <div className="container">
            <Box className={classes.inComponentButton}>
                <Button 
                    component={Link} 
                    to="/Success"
                >
                    Submit Survey
                    <AddBoxOutlinedIcon />
                </Button>
            </Box>
            <h1>This is Confirm</h1>
        </div>
    )
}

export default Confirm