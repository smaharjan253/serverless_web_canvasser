import React from 'react'
import { Box, Button, Hidden, Typography, FormControl, RadioGroup, FormControlLabel, Radio } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle6 from '../component-styles/QuestionsBannerStyle6';

const Question6 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className="questionstyle-2">
            <Hidden smDown>
                <QuestionBannerStyle6/>
            </Hidden>
            <FormControl component="fieldset" className={classes.formControl}>
                <Typography variant="h4" color="primary">
                    Are you voting for our party?
                </Typography>
                <br/>
                <RadioGroup value={props.value} onChange={props.handleChange}>
                    <FormControlLabel value="yes" control={<Radio />} label="Yes" />
                    <FormControlLabel value="no" control={<Radio />} label="No" />
                    <FormControlLabel value="undecided" control={<Radio />} label="Undecided" />
                </RadioGroup>
                <br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </FormControl>
        </Box>
    )
}

export default Question6