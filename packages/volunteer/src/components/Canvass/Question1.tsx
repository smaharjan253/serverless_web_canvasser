import React from 'react'
import { Box, List, Button, TextField, Hidden, Typography, Select, MenuItem } from '@material-ui/core'
import userDetails from '../component-styles/UserDetails'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle1 from '../component-styles/QuestionsBannerStyle1';

const Question1 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    return(
        <Box className={classes.questionBoxStyle}>
            <Hidden smDown>
                <QuestionBannerStyle1/>
            </Hidden>
            <form>
                <div className="questionstyle-1">
                <label style={{fontWeight: "bold"}}>Suburb: </label>
                
                <Select className="selectSuburb">{userDetails.suburb.length===1 ? 
                        <List>
                            <MenuItem>{userDetails.suburb[0]}</MenuItem>
                        </List>
                    :
                    userDetails.suburb.length===2 ?
                        <List>
                            <MenuItem>{userDetails.suburb[0]}</MenuItem>
                            <MenuItem>{userDetails.suburb[1]}</MenuItem>
                        </List>
                    :
                    userDetails.suburb.length===3 ?
                        <List>
                            <MenuItem>{userDetails.suburb[0]}</MenuItem>
                            <MenuItem>{userDetails.suburb[1]}</MenuItem>
                            <MenuItem>{userDetails.suburb[2]}</MenuItem>
                        </List>
                    :
                        <List>
                            <MenuItem>{userDetails.suburb[0]}</MenuItem>
                            <MenuItem>{userDetails.suburb[1]}</MenuItem>
                            <MenuItem>{userDetails.suburb[2]}</MenuItem>
                            <MenuItem>{userDetails.suburb[3]}</MenuItem>
                        </List>
                    
                    }
                    </Select>
                <br/><br/>
                <label style={{fontWeight: "bold"}}>Street Name: </label>
                <TextField 
                    variant="outlined" 
                    className="streetInput" 
                    placeholder="Enter Street Name"
                    // value={props.value.streetName}
                    // onChange={props.handleChange('streetName')}
                />
                <br/><br/>
                <label style={{fontWeight: "bold"}}>Household Address: </label>
                <TextField 
                    variant="outlined" 
                    className="houseInput" 
                    placeholder="Enter Household Address"
                    // value={props.value.houseName}
                    // onChange={props.handleChange('houseName')}
                />
                <br/><br/>
                <Button 
                    className="nextButton" 
                    size="large" 
                    variant="contained" 
                    color="primary"
                    onClick={toContinue}
                >
                    Next
                </Button>
                </div> 

            </form>
        </Box>
    )
}

export default Question1