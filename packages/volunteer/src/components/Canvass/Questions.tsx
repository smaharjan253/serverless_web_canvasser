import React, {useState} from 'react'
import useStyles from '../component-styles/NavBarStyle'
import Survey from './Survey'
import Question1 from './Question1'
import Question2 from './Question2'
import Question3 from './Question3'
import Question4 from './Question4'
import Question5 from './Question5'
import Question6 from './Question6'
import Question7 from './Question7'
import Confirm from './Confirm'
import Success from './Success'

const Questions = () => {
    const classes = useStyles();
    const [ step, setStep ] = useState(1)
    const [ question, setQuestion ] = useState('This is a Question')
    const [ answer, setAnswer ] = useState('')
    const [values, setValues] = useState()

    function nextStep(){
        setStep(newStep => step + 1)
    }

    function prevStep(){
        setStep(lastStep => step - 1)
    }

    const handleChange = (e: any) => {
        setAnswer(e.target.value);
      }
      
    return(
        <div className="container">
            {step === 1 ? 
                <div>
                    <Question1 
                        nextStep = {nextStep}
                        handleChange = {handleChange}
                        value = {values}
                    /> 
                </div> : 
            step === 2 ?
                    <Question2
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  : 
            step === 3 ?
                    <Question3
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
            step === 4 ?
                    <Question4
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
            step === 5 ?
                    <Question5
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
            step === 6 ?
                    <Question6
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
            step === 7 ?
                    <Question7
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
            step === 8 ?
                    <Survey
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />  :
                    <Confirm
                        nextStep = {nextStep}
                        prevStep = {prevStep}
                        handleChange = {handleChange}
                        value = {values}
                    />}
        </div>
    )
}


export default Questions