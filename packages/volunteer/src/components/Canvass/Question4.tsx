import React from 'react'
import { Box, Button, Hidden, Typography, Select, MenuItem } from '@material-ui/core'
import useStyles from '../component-styles/NavBarStyle';
import QuestionBannerStyle4 from '../component-styles/QuestionsBannerStyle4';

const Question4 = (props: any) => {
    const classes = useStyles();
    const toContinue = (e:any) => {
        e.preventDefault();
        props.nextStep();
    }
    const toReturn = (e:any) => {
        e.preventDefault();
        props.prevStep();
    }
    return(
        <Box className="questionstyle-2">
            <Hidden smDown>
                <QuestionBannerStyle4/>
            </Hidden>
            <form>
                <Typography variant="h4" color="primary">Employment Status</Typography>
                <br/><br/>
                <br/>
                <Select className="selectSuburb">
                    <MenuItem>Enterpreneurship</MenuItem>
                    <MenuItem>Science and Engineering</MenuItem>
                    <MenuItem>Doctor</MenuItem>
                    <MenuItem>Accountant</MenuItem>
                    <MenuItem>Architect</MenuItem>
                    <MenuItem>Hospitality</MenuItem>
                    <MenuItem>Retail</MenuItem>
                    <MenuItem>Military</MenuItem>
                    <MenuItem>Construction</MenuItem>
                    <MenuItem>Transportation</MenuItem>
                </Select>
                <br/><br/>
                <br/>
                <div className={classes.root2}>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toReturn}
                    >
                        Previous
                    </Button>
                    <Button 
                        className="nextButton" 
                        size="large" 
                        variant="contained" 
                        color="primary"
                        onClick={toContinue}
                    >
                        Next
                    </Button>
                </div>
            </form>
        </Box>
    )
}

export default Question4