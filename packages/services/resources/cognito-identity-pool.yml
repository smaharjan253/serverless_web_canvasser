Resources:
  # The federated identity for our user pool to auth with
  WebCanvasserCognitoIdentityPool:
    Type: AWS::Cognito::IdentityPool
    Properties:
      # Don't allow unathenticated users
      AllowUnauthenticatedIdentities: false
      # Link to our User Pool
      CognitoIdentityProviders:
        - ClientId:
            Ref: CognitoUserPoolWebCanvasserClient
          ProviderName:
            Fn::GetAtt: ["CognitoUserPoolWebCanvasser", "ProviderName"]
      # Generate a name based on the stage
      IdentityPoolName: ${self:custom.stage}IdentityPool

  # IAM roles
  WebCanvasserCognitoIdentityPoolRoles:
    Type: AWS::Cognito::IdentityPoolRoleAttachment
    Properties:
      IdentityPoolId:
        Ref: WebCanvasserCognitoIdentityPool
      Roles:
        authenticated:
          Fn::GetAtt: [WebCanvasserCognitoAuthRole, Arn]
        unauthenticated:
          Fn::GetAtt: [WebCanvasserCognitoUnAuthRole, Arn]

  # IAM role used for unauthenticated users
  WebCanvasserCognitoUnAuthRole:
    Type: AWS::IAM::Role
    Properties:
      Path: /
      RoleName: ${self:service.name}-${self:custom.stage}-unauth-Role
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Federated: "cognito-identity.amazonaws.com"
            Action:
              - "sts:AssumeRoleWithWebIdentity"
            Condition:
              StringEquals:
                "cognito-identity.amazonaws.com:aud":
                  Ref: WebCanvasserCognitoIdentityPool
              "ForAnyValue:StringLike":
                "cognito-identity.amazonaws.com:amr": unauthenticated
      Policies:
        - PolicyName: "CognitoUnAuthorizedPolicy"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: "Allow"
                Action:
                  - "mobileanalytics:PutEvents"
                  - "cognito-sync:*"
                Resource: "*"

                
  # IAM role used for authenticated users
  WebCanvasserCognitoAuthRole:
    Type: AWS::IAM::Role
    Properties:
      Path: /
      RoleName: ${self:service.name}-${self:custom.stage}-Admin-Role
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Federated: "cognito-identity.amazonaws.com"
            Action:
              - "sts:AssumeRoleWithWebIdentity"
            Condition:
              StringEquals:
                "cognito-identity.amazonaws.com:aud":
                  Ref: WebCanvasserCognitoIdentityPool
              "ForAnyValue:StringLike":
                "cognito-identity.amazonaws.com:amr": authenticated
      Policies:
        - PolicyName: "CognitoAuthorizedPolicy"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: "Allow"
                Action:
                  - "mobileanalytics:PutEvents"
                  - "cognito-sync:*"
                  - "cognito-identity:*"
                Resource: "*"

              # Allow users to invoke our API
              - Effect: "Allow"
                Action:
                  - "execute-api:Invoke"
                Resource:
                  Fn::Join:
                    - ""
                    - - "arn:aws:execute-api:"
                      - Ref: AWS::Region
                      - ":"
                      - Ref: AWS::AccountId
                      - ":"
                      - Ref: ApiGatewayRestApi
                      - "/*"
              
              # Allow users to list all bucket        
              - Sid: "AllowListingOfAllBucket"
                Effect: "Allow"
                Action: 
                  - "s3:ListAllMyBuckets"
                  - s3:GetBucketLocation
                Resource:
                  [
                    "arn:aws:s3:::*"
                  ]        
              # Allow users to list root level content of bucket        
              - Sid: "AllowRootLevelListingOfBucket"
                Effect: "Allow"
                Action: 
                  - "s3:ListBucket"
                Resource:
                  [
                    "arn:aws:s3:::webcanvasserleader",
                    "arn:aws:s3:::webcanvasser-strategist",
                    "arn:aws:s3:::webcanvasser-volunteer",
                    "arn:aws:s3:::webcanvasser-manager",
                  ]
                Condition:
                  StringEquals:
                    s3:prefix: [""]
                    s3:Delimiter: ["/"]


              # Allow users to list public folder content
              - Effect: "Allow"
                Action:
                  - "s3:ListBucket"
                Resource:
                  [
                    "arn:aws:s3:::webcanvasserleader",
                    "arn:aws:s3:::webcanvasser-strategist",
                    "arn:aws:s3:::webcanvasser-volunteer",
                    "arn:aws:s3:::webcanvasser-manager",
                  ]
                Condition:
                  StringLike:
                    s3:prefix: ["public/*"]

              # Allow to get object of public folder
              - Effect: "Allow"
                Action:
                  - "s3:*"
                Resource:
                  [
                    "arn:aws:s3:::webcanvasserleader/public/*",
                    "arn:aws:s3:::webcanvasser-strategist/public/*",
                    "arn:aws:s3:::webcanvasser-volunteer/public/*",
                    "arn:aws:s3:::webcanvasser-manager/public/*",
                  ]

# Print out the Id of the Identity Pool that is created
Outputs:
  IdentityPoolId:
    Value:
      Ref: WebCanvasserCognitoIdentityPool
  RoleId:
    Value:
      Ref: WebCanvasserCognitoAuthRole
