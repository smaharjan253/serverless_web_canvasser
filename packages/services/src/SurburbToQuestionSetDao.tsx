import { v1 } from 'uuid'
import AWS from 'aws-sdk'
const tableName = 'SurburbToQuestionSet';
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const dynamoDb1 = new AWS.DynamoDB();
function unique(arr) {
    return Array.from(new Set(arr))
}
export const addQuestionSetToSurburb = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const surburb = data.surburb;
    const questionContent = data.questionContent;
    const params = {
        TableName: tableName,
        // AttributeDefinitions:attributeDefinitions,
        Item: {
            id: v1(),
            surburb: surburb,
            questionContent: questionContent
        }
    };
    dynamoDb.put(params, (error, data) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain',"Access-Control-Allow-Origin": "*"  },
                body: 'Couldn\'t create the account.',
            });
            return;
        }
        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(params.Item),
            headers:{"Access-Control-Allow-Origin": "*" }
        };
        callback(null, response, params.Item);
    });

};

export const getQustionSetBySurburbs = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const surburbs = data.surburbs;
    const params = {
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' ,"Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        const result2 = result.Items.filter((item)=>{
            if(surburbs.indexOf(item.surburb)>-1){
                return 1
            }else{
                return 0
            }
        })
        //check whether account exists
        if (result.Count > 0) {
            const response = {
                statusCode: 200,
                body: JSON.stringify(result2),
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null, response);
        } else {
            const response = {
                statusCode: 201,
                body: {
                    message: 'cannot find account in database'
                },
            };
            callback(null,response)
        }
    });
}
