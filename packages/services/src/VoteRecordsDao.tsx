import { v1 } from 'uuid'
import AWS from 'aws-sdk'
const tableName = 'VoteRecord';
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const dynamoDb1 = new AWS.DynamoDB();
function unique(arr) {
    return Array.from(new Set(arr))
}
export const createRecord = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const surburb = data.surburb;
    const ifPositive = data.ifPositive;
    const volunteerId = data.volunteerId;
    const createTime = new Date().getTime();
    const state1 = data.state;
    const questionRecords = data.questionRecords;
    const successful = data.successful;
    const volunteerName = data.volunteerName;
    const policyName = data.policyName;
    const ageGroup = data.ageGroup
    const gender = data.gender
    const employment = data.employment
    const month = data.month
    const params = {
        TableName: tableName,
        // AttributeDefinitions:attributeDefinitions,
        Item: {
            id: v1(),
            state1: state1,
            surburb: surburb,
            ifPositive: ifPositive,
            volunteerId: volunteerId,
            volunteerName: volunteerName,
            createTime: createTime,
            questionRecords: questionRecords,
            successful: successful,
            policyName: policyName,
            ageGroup:ageGroup,
            gender:gender,
            employment:employment,
            month:month,
        }
    };
    dynamoDb.put(params, (error, data) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain', "Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t create the account.',
            });
            return;
        }
        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(params.Item),
            headers: { "Access-Control-Allow-Origin": "*" }
        };
        callback(null, response, params.Item);
    });

};

export const getRecordsBySurburbs = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const surburbs = data.surburbs;
    const params = {
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain', "Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        console.log('enter1')
        const result2 = result.Items.filter((item) => {
            if (surburbs.indexOf(item.surburb) > -1) {
                return 1
            } else {
                return 0
            }
        })
        const response = {
            statusCode: 200,
            body: JSON.stringify(result2),
            headers: { "Access-Control-Allow-Origin": "*" }
        };
        callback(null, response);
    });
}

export const getRecordsByFourPara = (event, context, callback) => {
    const data = JSON.parse(event.body);
    var situa = 0;
    var params;
    const startDate = data.startDate == undefined ? 0 : parseInt(data.startDate);
    const endDate = data.endDate == undefined ? Number.MAX_SAFE_INTEGER : parseInt(data.endDate);
    situa += data.surburb == undefined ? 0 : 1;
    situa += data.state == undefined ? 0 : 2;
    const surburb: string = data.surburb;
    const state1: string = data.state;
    switch (situa) {
        case 0:
            params = {
                ExpressionAttributeValues: {
                    ':startDate': startDate,
                    ':endDate': endDate,
                },
                FilterExpression: 'createTime between :startDate and :endDate',
                TableName: tableName
            };
            break;
        case 1:
            params = {
                ExpressionAttributeValues: {
                    ':surburb': surburb,
                    ':startDate': startDate,
                    ':endDate': endDate,
                },
                FilterExpression: 'surburb = :surburb and createTime between :startDate and :endDate',
                TableName: tableName
            };
            break;
        case 2:
            params = {
                ExpressionAttributeValues: {
                    ':state1': state1,
                    ':startDate': startDate,
                    ':endDate': endDate,
                },
                FilterExpression: 'surburb = :surburb and state1=:state1 and createTime between :startDate and :endDate',
                TableName: tableName
            };
            break;
        case 3:
            params = {
                ExpressionAttributeValues: {
                    ':surburb': surburb,
                    ':state1': state1,
                    ':startDate': startDate,
                    ':endDate': endDate,
                },
                FilterExpression: 'surburb = :surburb and state1=:state1 and createTime between :startDate and :endDate',
                TableName: tableName
            };
            break;
    }

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain', "Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        //check whether account exists
        if (result.Count > 0) {
            const response = {
                statusCode: 200,
                body: JSON.stringify(result),
                headers: { "Access-Control-Allow-Origin": "*" }
            };
            callback(null, response);
        } else {
            const response = {
                statusCode: 201,
                body: {
                    message: 'cannot find account in database'
                },
                headers: { "Access-Control-Allow-Origin": "*" }
            };
            callback(null, response)
        }
    });
}

export const getRecordsByVolunteerId = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const volunteerId = data.volunteerId;
    const params = {
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain', "Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        console.log('enter1')
        const result2 = result.Items.filter((item) => {
            if (volunteerId === item.volunteerId) {
                return 1
            } else {
                return 0
            }
        })
        const response = {
            statusCode: 200,
            body: JSON.stringify(result2),
            headers: { "Access-Control-Allow-Origin": "*" }
        };
        callback(null, response);
    });
}