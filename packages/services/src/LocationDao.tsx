import { v1 } from 'uuid'
import AWS from 'aws-sdk'
const tableName = 'Location';
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const dynamoDb1 = new AWS.DynamoDB();
function unique (arr) {
    return Array.from(new Set(arr))
  }
export const addLocation = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const country = data.country;
    const state = data.state;
    const surburbs = data.surburbs;
    const records = surburbs.map((surburb) => {
        return {
            PutRequest: {
                Item: {
                    "id": { "S": v1() },
                    "State1": { "S": state },
                    "country": { "S": country },
                    'surburb': { 'S': surburb }
                }
            }
        }
    });
    var params = {
        RequestItems: {
            'Location': records
        }
    };
    dynamoDb1.batchWriteItem(params, (error, data) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain',"Access-Control-Allow-Origin": "*"  },
                body: 'Couldn\'t create the account.',
            });
            return;
        }
        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(records),
            headers:{"Access-Control-Allow-Origin": "*" }
        };
        callback(null, response, records);
    });

};

export const deleteOneSurburb = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const id: string = data.id;
    const params = {
        TableName: tableName,
        Key: {
            id: id
        },
    };
    dynamoDb.delete(params, (error, data) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain',"Access-Control-Allow-Origin": "*"  },
                body: 'Couldn\'t remove the todo item.',
            });
            return;
        }
        //start to request to delete something according to userType

        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify({ data }),
            headers:{"Access-Control-Allow-Origin": "*" }
        };
        callback(null, response);
    });

}

export const updateOneSurburb = (event, context, callback) => {
    const timestamp = new Date().getTime();
    const data = JSON.parse(event.body);
    
    const params = {
        TableName: tableName,
        Key: {
            id: data.id,
        },
        ExpressionAttributeValues: {
            ':State1': data.state,
            ':country': data.country,
            ':surburb': data.surburb,

        },
        UpdateExpression: 'SET State1 = :State1, country = :country, surburb = :surburb',
        ReturnValues: 'ALL_NEW',
    };

    // update the todo in the database
    dynamoDb.update(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' ,"Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }

        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(result),
            headers:{"Access-Control-Allow-Origin": "*" }
        };
        callback(null, response);
    });
}

export const getStatesByCountry = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const country: string = data.country;
    const params = {
        ExpressionAttributeValues: {
            ':country': country,
        },
        FilterExpression: 'country = :country',
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain',"Access-Control-Allow-Origin": "*"  },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        //get state out of result
        const states = result.Items.map((item) => {
            return item.State1
        })
        //check whether account exists
        if (result.Count > 0) {
            const response = {
                statusCode: 200,
                body: JSON.stringify(unique(states)),
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null, response);
        } else {
            const response = {
                statusCode: 201,
                body: {
                    message: 'cannot find account in database'
                },
                headers:{"Access-Control-Allow-Origin": "*" }
            };
        }
    });
}

export const getSurburbsByStateAndCountry = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const country: string = data.country;
    const state: string = data.state;
    const params = {
        ExpressionAttributeValues: {
            ':country': country,
            ':State1': state,
        },
        FilterExpression: 'country = :country and State1=:State1',
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' ,"Access-Control-Allow-Origin": "*" },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        //get state out of result
        const states = result.Items.map((item) => {
            return item.surburb
        })
        //check whether account exists
        if (result.Count > 0) {
            const response = {
                statusCode: 200,
                body: JSON.stringify(states),
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null, response);
        } else {
            const response = {
                statusCode: 201,
                body: {
                    message: 'cannot find account in database'
                },
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null,response)
        }
    });
}

export const getAllLocations = (event, context, callback) => {
    const params = {
        TableName: tableName
    };

    // fetch todo from the database
    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain',"Access-Control-Allow-Origin": "*"  },
                body: 'Couldn\'t fetch the todo item.',
            });
            return;
        }
        
        //check whether account exists
        if (result.Count > 0) {
            const response = {
                statusCode: 200,
                body: JSON.stringify(result),
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null, response);
        } else {
            const response = {
                statusCode: 201,
                body: {
                    message: 'cannot find account in database'
                },
                headers:{"Access-Control-Allow-Origin": "*" }
            };
            callback(null,response)
        }
    });
}