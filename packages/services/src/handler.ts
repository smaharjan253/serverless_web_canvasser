import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }, null, 2),
  };
}

export const postAuth = async (event, _context, callback) => {
  console.log(event)
      // Send post authentication data to Cloudwatch logs
      console.log ("Authentication successful");
      console.log ("Trigger function =", event.triggerSource);
      console.log ("User pool = ", event.userPoolId);
      console.log ("App client ID = ", event.callerContext.clientId);
      console.log ("User ID = ", event.userName);
  
      // Return to Amazon Cognito
      callback(null, event);
}
