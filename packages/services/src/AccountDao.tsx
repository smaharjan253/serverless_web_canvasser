import { v1 } from 'uuid';
import AWS from 'aws-sdk';
import * as dynamoDbLib from './libs/dynamodb-lib';
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const tableName = 'Account';

export const createAccount = async (event, context, callback) => {
	console.log(event);

	const timestamp = new Date().getTime();
	const data = event.request.userAttributes;
	const params = {
		TableName: tableName,
		// AttributeDefinitions:attributeDefinitions,
		Item: {
			id: data.sub,
			firstname: data.given_name,
			lastname: data.family_name,
			email: data.email,
			birthdate: data.birthdate,
			contactnumber: data.phone_number,
			secretcode: data['custom:secretcode'],
			usertype: data['custom:usertype'],
			suburbs: [ data['custom:suburb1'], data['custom:suburb2'], data['custom:suburb3'], data['custom:suburb4'] ],
			created: timestamp,
			updated: timestamp
		}
	};
	await dynamoDbLib.call('put', params);
	callback(null, event);
};

export const deleteAccount = (event, context, callback) => {
	const data = JSON.parse(event.body);
	const id: string = data.id;
	const params = {
		TableName: tableName,
		Key: {
			id: id
		}
	};
	dynamoDb.delete(params, (error) => {
		// handle potential errors
		if (error) {
			console.error(error);
			callback(null, {
				statusCode: error.statusCode || 501,
				headers: {
					'Content-Type': 'text/plain',
					'Access-Control-Allow-Origin': '*'
				},
				body: "Couldn't remove the todo item."
			});
			return;
		}
		//start to request to delete something according to usertype

		// create a response
		const response = {
			statusCode: 200,
			body: JSON.stringify({}),
			headers: { 'Access-Control-Allow-Origin': '*' }
		};
		callback(null, response);
	});
};

export const queryOneAccount = (event, context, callback) => {
	const data = JSON.parse(event.body);
	const email: string = data.email;
	const password: string = data.password;
	const params = {
		ExpressionAttributeValues: {
			':email': email,
			':password': password
		},
		FilterExpression: 'email = :email and password = :password',
		TableName: tableName
	};

	// fetch todo from the database
	dynamoDb.scan(params, (error, result) => {
		// handle potential errors
		if (error) {
			console.error(error);
			callback(null, {
				statusCode: error.statusCode || 501,
				headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
				body: "Couldn't fetch the todo item."
			});
			return;
		}
		//check whether account exists
		if (result.Count > 0) {
			const response = {
				statusCode: 200,
				body: JSON.stringify(result.Items),
				headers: { 'Access-Control-Allow-Origin': '*' }
			};
			callback(null, response);
		} else {
			const response = {
				statusCode: 201,
				body: {
					message: 'cannot find account in database'
				},
				headers: { 'Access-Control-Allow-Origin': '*' }
			};
			callback(null, response);
		}
	});
};

export const getAllAccountBySurburbs = (event, context, callback) => {
	const data = JSON.parse(event.body);
	const suburbs: string[] = data.suburbs;
	// fetch all todos from the database

	const params = {
		ExpressionAttributeValues: {
			':usertype': 'Volunteers'
		},
		FilterExpression: 'usertype = :usertype',
		TableName: tableName
	};
	dynamoDb.scan(params, (error, result) => {
		// handle potential errors
		if (error) {
			console.error(error);
			callback(null, {
				statusCode: error.statusCode || 501,
				headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
				body: "Couldn't fetch the todos."
			});
			return;
		}
		console.log(result);
		const result2 = result.Items.filter((item) => {
			if (!('suburbs' in item)) {
				return 0;
			}
			const surburbCount = item.suburbs.length;
			for (var i = 0; i < surburbCount; i++) {
				const surburbName = item.suburbs[i];
				if (suburbs.indexOf(surburbName) > -1) {
					return 1;
				}
			}
			return 0;
		});
		console.log(result2);
		// create a response
		const response = {
			statusCode: 200,
			body: JSON.stringify(result2),
			headers: { 'Access-Control-Allow-Origin': '*' }
		};
		callback(null, response);
	});
};

export const updateOneAccount = (event, context, callback) => {
	const timestamp = new Date().getTime();
	const data = JSON.parse(event.body);

	const params = {
		TableName: tableName,
		Key: {
			id: data.id
		},
		ExpressionAttributeValues: {
			':fullName': data.fullName,
			':age': data.age,
			':contactNumber': data.contactNumber,
			':email': data.email,
			':password': data.password,
			':usertype': data.usertype,
			':createTime': timestamp,
			':enabled': '1',
			':suburbs': data.suburbs
		},
		UpdateExpression:
			'SET fullName = :fullName, age = :age, contactNumber = :contactNumber,\
    email = :email, password = :password, usertype = :usertype,\
    createTime = :createTime, enabled = :enabled, suburbs = :suburbs',
		ReturnValues: 'ALL_NEW'
	};

	// update the todo in the database
	dynamoDb.update(params, (error, result) => {
		// handle potential errors
		if (error) {
			console.error(error);
			callback(null, {
				statusCode: error.statusCode || 501,
				headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
				body: "Couldn't fetch the todo item."
			});
			return;
		}

		// create a response
		const response = {
			statusCode: 200,
			body: JSON.stringify(result.Attributes),
			headers: { 'Access-Control-Allow-Origin': '*' }
		};
		callback(null, response);
	});
};

export const getInfoByEmail = (event, context, callback) => {
	const data = JSON.parse(event.body);
	const params = {
		ExpressionAttributeValues: {
			':email': data.email
		},
		FilterExpression: 'email = :email',
		TableName: tableName
	};

	// fetch todo from the database
	dynamoDb.scan(params, (error, result) => {
		// handle potential errors
		if (error) {
			console.error(error);
			callback(null, {
				statusCode: error.statusCode || 501,
				headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
				body: "Couldn't fetch the todo item."
			});
			return;
		}
		//check whether account exists
		if (result.Count > 0) {
			const response = {
				statusCode: 200,
				body: JSON.stringify(result.Items),
				headers: { 'Access-Control-Allow-Origin': '*' }
			};
			callback(null, response);
		} else {
			const response = {
				statusCode: 201,
				body: {
					message: 'cannot find account in database'
				},
				headers: { 'Access-Control-Allow-Origin': '*' }
			};
			callback(null, response);
		}
	});
};
