import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

module.exports.main = async (event) => {
    const data = JSON.parse(event.body)
    const params = {
      TableName: process.env.DYNAMODB_TABLE,
      Item: {
        email: data.email,
      }
    }
  
    try {
      const result = await dynamoDbLib.call("get", params);
      if (result.Item) {
        // Return the retrieved item
        return success(result.Item);
      } else {
        return failure({ status: false, error: "Item not found." });
      }
    } catch (e) {
      console.log(e)
      return failure({ status: false, })
    }
  }