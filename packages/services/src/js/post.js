const AWS = require("aws-sdk");

AWS.config.update({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.REGION
});

const cognitoISP = new AWS.CognitoIdentityServiceProvider()
const params = {
  "UserAttributes": [
    {
      "Name": "given_name",
      "Value": "aaa"
    }, {
      "Name": "family_name",
      "Value": "bbb"
    }, {
      "Name": "email",
      "Value": "b@a.com"
    }, {
      "Name": "birthdate",
      "Value": "10/10/2020"
    }, {
      "Name": "phone_number",
      "Value": "+6112345967809"
    }
  ],
  "Username": "b@a.com",
  "UserPoolId": "ap-southeast-2_FdFmS403b",
}
async (params) => {
  try {
    const res = await cognitoISP.adminCreateUser(params).promise()
    console.log({ res });
    _context.done(undefined, res)
  } catch (error) {
    console.log(error);
  }
}
