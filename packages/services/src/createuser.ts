import AWS from "aws-sdk";

export const main = async (event: any, _context: any) => {
  console.log(event);

  const data = event.request.userAttributes  
  const opt = {
    GroupName: data['custom:usertype'],
    UserPoolId: event.userPoolId,
    Username: event.userName
  };

  try {
    if (data["cognito:user_status"] === "CONFIRMED") {
      const cognitoISP = new AWS.CognitoIdentityServiceProvider();
      await cognitoISP.adminAddUserToGroup(opt).promise()
    }
    _context.done(undefined, event);
  } catch (e) {
    console.error(e);
  }
}

