import * as AWS from "aws-sdk";

export const main = async (event: any, _context: any) => {
    console.log(event);

    const cognitoISP = new AWS.CognitoIdentityServiceProvider()
    const data = JSON.parse(event.body);
    const params = {
        "UserAttributes": [
            {
                "Name": "given_name",
                "Value": data.firstname
            }, {
                "Name": "family_name",
                "Value": data.lastname
            }, {
                "Name": "email",
                "Value": data.email
            }, {
                "Name": "birthdate",
                "Value": data.birthdate
            }, {
                "Name": "phone_number",
                "Value": data.phone_number
            }
        ],
        "Username": data.email,
        "UserPoolId": 'ap-southeast-2_FdFmS403b',
    }

    try {
        const res = await cognitoISP.adminCreateUser(params).promise()
        console.log(res);
        
        _context.done(undefined, res)
    } catch (error) {
        console.log(error);

    }
}