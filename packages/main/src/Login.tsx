import React, { SyntheticEvent } from 'react';
import { Container, CssBaseline, Divider, Paper, createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import useStyles from '@canvasser/components/src/styles';
import ValidationTextField from '@canvasser/components/src/ValidationTextField';
import { useForm } from '@canvasser/components/src/useForm';
import { useHistory } from 'react-router-dom';
import Logo from './public/logo.svg';
import CustomizedButton from './CustomizedButton';
import { Auth } from 'aws-amplify';
import * as AWS from 'aws-sdk';

const Login: React.FC = () => {
	const classes = useStyles();
	const history = useHistory();

	const { values, useInput, isValid } = useForm({
		email: '',
		password: ''
	});

	const handleSubmit = async (event: SyntheticEvent) => {
		event.preventDefault();
		const result = await Auth.signIn(values.email, values.password);
		console.log(result);

		const group = result.signInUserSession.idToken.payload['cognito:groups'];
		const groupName = group[0];

		switch (groupName) {
			case 'Admin':
					window.open("http://d3g7au8wbw4np8.cloudfront.net", '_self')
				break
			case 'Managers':
					window.open("http://d3g7au8wbw4np8.cloudfront.net", '_self')
				break
			case 'Strategists':
					window.open("http://d3g7au8wbw4np8.cloudfront.net", '_self')
				break
			case 'Volunteers':
					window.open("http://d3g7au8wbw4np8.cloudfront.net", '_self')
				break
			
		}
	};


	const handleForget = (event: SyntheticEvent) => {
		event.preventDefault();
		console.log('enter1');

		// signup();
		history.push('/forget_password');
	};

	const theme = createMuiTheme({
		palette: {
			primary: {
				main: '#1867AC'
			}
		},
		overrides: {
			MuiButton: {
				root: {
					background: 'white',
					borderRadius: 5,
					borderWidth: 5,
					padding: '0 30px',
					margin: '16px',
					color: '#1867AC', //textcolor
					fontWeight: 'bold',
					height: 48
				}
			},
			MuiDivider: {
				root: {
					height: 5,
					width: '25px',
					backgroundColor: '#ACD5F9',
					margin: '16px',
					border: 0,
					borderRadius: 5
				}
			}
		}
	});
	return (
		<div className={classes.root}>
			<CssBaseline />
			<Container component="main" maxWidth="xs">
				<Paper className={classes.paper}>
					<div className={classes.banner}>
						<Logo />
					</div>
					<div className={classes.form_container}>
						<form>
							<ValidationTextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="email"
								label="Email Address"
								name="email"
								autoComplete="email"
								autoFocus
								value={values.email}
								{...useInput('email', 'isRequired, isEmail')}
							/>

							<ValidationTextField
								variant="outlined"
								margin="normal"
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								value={values.password}
								autoComplete="current-password"
								{...useInput('password', {
									isRequired: true,
									isLength: {
										min: 6
									}
								})}
							/>
						</form>
						<MuiThemeProvider theme={theme}>
							<Divider component="ul" />
							<CustomizedButton
								type="submit"
								disabled={!isValid}
								fullWidth
								variant="contained"
								color="primary"
								className={classes.sub_button}
								onClick={handleSubmit}
							>
								Login
							</CustomizedButton>
							<CustomizedButton
								type="submit"
								color="primary"
								fullWidth
								variant="outlined"
								className={classes.forget_button}
								onClick={handleForget}
							>
								forget password
							</CustomizedButton>
						</MuiThemeProvider>
					</div>
				</Paper>
			</Container>
		</div>
	);
};

export default Login;
