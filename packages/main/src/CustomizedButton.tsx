import { Button, withStyles } from "@material-ui/core";

const CustomizedButton = withStyles({
    root: {
        '& input:valid + fieldset': {
            borderColor: '#ACD5F9',
            borderWidth: 2,
        },
        '& input:invalid + fieldset': {
            borderColor: 'red',
            borderWidth: 2,
        },
        '& input:valid:focus + fieldset': {
            borderColor: '#ACD5F9',
            padding: '4px !important', // override inline-style
        },
        '& .MuiButton-root': {
            '&:hover fieldset': {
                color: '#ACD5F9',
            },
        },
    },
})(Button);

export default CustomizedButton