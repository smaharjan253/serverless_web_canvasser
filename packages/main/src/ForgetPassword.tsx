import React from 'react';
import useStyles from '@canvasser/components/src/styles';
import ValidationTextField from '@canvasser/components/src/ValidationTextField';
import { useForm } from '@canvasser/components/src/useForm';
import {
	CssBaseline,
	Container,
	Divider,
	Typography,
	Paper,
	createMuiTheme,
	MuiThemeProvider
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import Logo from './public/logo.svg';
import CustomizedButton from './CustomizedButton';
import sendemail from '../../components/src/sendEmail';

const ForgetPassword: React.FC = () => {
	const classes = useStyles();
	const history = useHistory();

	const theme = createMuiTheme({
		palette: {
			primary: {
				main: '#1867AC'
			}
		},
		overrides: {
			MuiButton: {
				root: {
					background: 'white',
					borderRadius: 5,
					borderWidth: 5,
					padding: '0 30px',
					margin: '16px',
					color: '#1867AC', //textcolor
					fontWeight: 'bold',
					height: 48
				}
			},
			MuiDivider: {
				root: {
					height: 5,
					width: '25px',
					backgroundColor: '#ACD5F9',
					margin: '16px',
					border: 0,
					borderRadius: 5
				}
            },
            MuiTypography: {
                root: {
                    color: "#0A437C",
                    margin: '16px 0', 
                    textTransform: "uppercase"
                },
                body1: {
                    fontWeight: 'bold', 
                    fontSize:'14px'
                }
            }
		}
	});

	const { values, useInput, isValid } = useForm({
		email: ''
	});

	const handleSubmit = (event: any) => {
		event.preventDefault();
		console.log(values);
		// sendemail(values.email)
	};

	return (
		<div className={classes.root}>
			<CssBaseline />
			<Container component="main" maxWidth="xs">
				<Paper className={classes.paper}>
					<div className={classes.banner}>
						<Logo />
					</div>
					<div className={classes.form_container}>
						<form>
							<MuiThemeProvider theme={theme}>
								<Typography>Get Back Your Login Credentials</Typography>
							</MuiThemeProvider>

							<ValidationTextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="email"
								label="Email Address"
								name="email"
								autoComplete="email"
								autoFocus
								value={values.email}
								{...useInput('email', 'isRequired, isEmail')}
							/>
						</form>

						<MuiThemeProvider theme={theme}>
							<Divider component="ul" />
							<CustomizedButton
								type="submit"
								disabled={!isValid}
								fullWidth
								variant="contained"
								color="primary"
								className={classes.sub_button}
								onClick={handleSubmit}
							>
								send request
							</CustomizedButton>
						</MuiThemeProvider>
					</div>
				</Paper>
			</Container>
		</div>
	);
};

export default ForgetPassword;
