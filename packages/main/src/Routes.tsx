import React from 'react'
import { Route, Router as Router, Redirect } from 'react-router-dom'
import ForgetPassword from './ForgetPassword'
import Login from './Login'
import { createBrowserHistory } from "history"
import Amplify from 'aws-amplify';
import awsmobile from '@canvasser/components/src/aws-exports';

Amplify.configure(awsmobile);

const Routes: React.FC = () => {
    const customHistory = createBrowserHistory()
    
    return (
        <Router history={customHistory}>
            {/* <Redirect exact from="/" to='/login' /> */}
            <Route exact path='/' component={Login} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/forget_password' component={ForgetPassword} />
        </Router>
    )
}

export default Routes