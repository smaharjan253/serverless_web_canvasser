module.exports = {
    globals: {
        'ts-jest': {
            extends: './babel.config.js',
        },
    },
    moduleFileExtensions: ['ts', 'tsx', 'js'],
    roots: ["<rootDir>/src"],
    testMatch: ["**/__tests__/**/*.+(ts|tsx|js)", "**/?(*.)+(spec|test).+(ts|tsx|js)"],
    transform: {
        "^.+\\.(ts|tsx)$": "ts-jest",
        "^.+\\.(svg)$": "<rootDir>/src/__test__/fileTransformer.js"
    }
}