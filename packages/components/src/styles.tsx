import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
        background: '#0A437C',
    },

    paper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        width: theme.spacing(58.5),
        paddingTop: theme.spacing(5),
        marginLeft: theme.spacing(-5),
        background: 'white'
    },
    banner: {
        display: 'flex',
        flexDirection: 'column',
        height: 120,
        background: "#F2F2F2",
        alignItems: 'center',
        justifyContent: 'center',
    },
    form_container: {
        padding: theme.spacing(11),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    divider: {
        height: 5,
        width: theme.spacing(4),
        background: "#ACD5F9",
        margin: theme.spacing(4),
        border: 0,
        borderRadius: 5
    },
    sub_button: {
        background: '#1867AC',
        borderRadius: 5,
        width: '100%',
        color: 'white', //textcolor
        fontWeight: 'bold',
        height: theme.spacing(6.5),
        padding: '0 30px',
    },
    forget_button: {
        background: 'white',
        borderRadius: 5,
        borderWidth: 2,
        padding: '0 30px',
        color: "#1867AC",   //textcolor
        fontWeight: 'bold',
        height: theme.spacing(6.5),
    }

}))

export default useStyles