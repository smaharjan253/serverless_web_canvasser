import emailjs from 'emailjs-com';
import constant from './constant'

const sendemail = (inputs: any, callback: any) => {
    const replyTo = inputs.replyTo;
    const VolunteerName = inputs.VolunteerName;
    const account = inputs.account;
    const password = inputs.password;
    const templateId = inputs.Type>0?constant.templateIdForForgot:constant.templateIdForNew;
    const serviceId = constant.serviceId;
    const userId = constant.userId;
    emailjs.init(userId);
    emailjs.send(
        serviceId, 
        templateId,
        {reply_to:replyTo , VolunteerName:VolunteerName,account:account, password:password}
    ).then(res => {
        console.log('Email successfully sent!');
        // callback();
    })
        // Handle errors here however you like, or use a React error boundary
        .catch(err => console.error('Oh well, you failed. Here some thoughts on the error that occured:', err));
}
export default sendemail