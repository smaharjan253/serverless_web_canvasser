const constant = {
    //parameters for sending email
    templateIdForNew : '3',
    templateIdForForgot : '3',
    serviceId : 'gmail',
    userId : 'user_L9TRRUAYk2vIQ6FGFtaUO',

    //urls
    forgotPasswordUrl:'https://55m4y9l7jj.execute-api.us-east-1.amazonaws.com/dev/getInfoByEmail',
    requestRecordsUrl:'https://egekn2dbp4.execute-api.us-east-1.amazonaws.com/dev/getRecordsBySurburbs',
    getAllAccountBySurburbsUrl:'https://egekn2dbp4.execute-api.us-east-1.amazonaws.com/dev/getAllAccountBySurburbs',
};
export default constant;