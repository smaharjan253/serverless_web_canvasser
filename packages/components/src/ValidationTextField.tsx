import { TextField, withStyles } from "@material-ui/core";

const ValidationTextField = withStyles({
    root: {
        '& input:valid + fieldset': {
            borderColor: '#ACD5F9',
            borderWidth: 2,
        },
        '& input:invalid + fieldset': {
            borderColor: 'red',
            borderWidth: 2,
        },
        '& input:valid:focus + fieldset': {
            borderColor: '#ACD5F9',
            padding: '4px !important', // override inline-style
        },
        '& .MuiOutlinedInput-root': {
            '&:hover fieldset': {
                borderColor: '#ACD5F9',
            },
        },
    },
})(TextField);

export default ValidationTextField