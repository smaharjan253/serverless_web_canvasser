import React, { PureComponent } from 'react'
import { PieChart, Pie, Sector, Cell } from 'recharts'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
const data = [
    //   [{ name: 'Positive', value: 400 },{name:'others',value:0}],
    //   [{ name: 'Negative', value: 300 },{name:'others',value:0}],
    //   [{ name: 'left', value: 300 },{name:'others',value:0}],
    { name: 'Positive', value: 400 },
    { name: 'Negative', value: 300 },
    { name: 'left', value: 300 }
];


const innerRadius = [40, 50, 60]
const outerRadius = [90, 80, 70]
const cx = 110;
const cy = 100;
const COLORS = ['#0088FE', '#00C49F', '#FFBB28'];
interface PieComponentProps {
    // successfulVisits: string;
    positiveVisits: number;
    negativeVisits: number;
    sum: number,
}
class PieChartComponent extends PureComponent<PieComponentProps> {

    render() {
        return (
            <PieChart width={250} height={120} >
               
                <g >

                    <text
                        fontSize={30}
                        fill="#FF5660"
                        x={cx} y={cy - 30} dy={10} textAnchor="middle" >{this.props.sum}</text>
                    <text
                        fontSize={10}
                        fill="#232425"
                        opacity={0.5}
                        x={cx} y={cy - 10} dy={10} textAnchor="middle" >Total Voters</text>
                    <text
                        fontSize={10}
                        fill="#232425"
                        x={0} y={cy - 7} dy={10}
                        opacity={0.5} textAnchor="start" >0</text>
                    <text
                        fontSize={10}
                        fill="#232425"
                        x={cx+100} y={cy - 7} dy={10}
                        opacity={0.5} textAnchor="start" >{this.props.sum}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={70}
                        outerRadius={98}
                        endAngle={180 - this.props.positiveVisits * 180 / this.props.sum}
                        startAngle={180}
                        fill="#FF5660"
                    // filter="url(#blurMe)"
                    />
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={70}
                        outerRadius={98}
                        startAngle={180 - this.props.positiveVisits * 180 / this.props.sum}
                        endAngle={180 - this.props.positiveVisits * 180 / this.props.sum - this.props.negativeVisits * 180 / this.props.sum}
                        fill="#F0F2F8"
                    />

                </g>
            </PieChart >
        );
    }
}

export default PieChartComponent