import React from 'react'
import { LineChart, XAxis, YAxis, CartesianGrid, Line, Tooltip, Legend } from 'recharts'
import { Typography, makeStyles } from '@material-ui/core'
interface LineGraphComponentProps{
    data:any,
}
const LineGraphComponent:React.FC<LineGraphComponentProps> = (props) => {
    const {data} = props
    const useStyles = makeStyles(theme => ({
        overViewHeader: {
            marginLeft: 10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            fontWeight: 700,
        },
    }));

    const classes = useStyles();
    return(
            <div>
                <Typography className={classes.overViewHeader}>
                    Campaign Forecast by Gender
                </Typography>
                <br/>
                <LineChart width={1000} height={300} data={data}>
                    <Legend/>
                    <XAxis dataKey="name"/>
                    <YAxis />
                    <CartesianGrid stroke="#eee" strokeDasharray="1000 100" vertical={false}/>
                    <Tooltip/>
                    
                    <Line name= "Positive" dataKey="postiveVotes" stroke="#00CCFF" fill="#00CCFF"/>
                    <Line name= "Negative" dataKey="negativeVotes" stroke="#6A0DAD" fill="#6A0DAD"/>
                </LineChart>
                
            </div>
            )
        }
        
export default LineGraphComponent