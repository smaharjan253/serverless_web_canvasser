import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./component-styles/NavBarStyle";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import ListIcon from "@material-ui/icons/List";

const DefaultQuestions = () => {
  const classes = useStyles();

  return (
    <div className="container3">
      <div style={{ display: "flex" }}>
        <h1> DEFAULT QUESTIONS </h1>
      </div>

      <form className="my-form">
        <ol>
          <div className="questionContainer-1">
            <li>
              Suburb:{" "}
              <input
                style={{
                  width: "200px",
                  height: "30px",
                  position: "relative",
                  left: "97px"
                }}
                type="text"
                name="suburbName"
                placeholder="Select Suburb"
                disabled
              />
            </li>
            <br />
            <label>Street Name:</label>
            <input
              style={{
                width: "200px",
                height: "30px",
                position: "relative",
                left: "55px"
              }}
              type="text"
              name="streetName"
              placeholder="Enter Address here"
              disabled
            />
            <br />
            <br />
            <label>Household Address:</label>
            <input
              style={{
                width: "200px",
                height: "30px",
                position: "relative",
                left: "-8px"
              }}
              type="text"
              name="householdAddress"
              placeholder="Enter Address here"
              disabled
            />
            <br />
          </div>

          <div className="questionContainer-1">
            <li>Is the voter available during the visit:</li>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> YES</label>
            </div>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> NO</label>
            </div>
            <br />
          </div>

          <div className="questionContainer-1">
            <li>Gender:</li>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> Male</label>
            </div>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> Female</label>
            </div>
            <div style={{ opacity: 0.5 }}>
              <br />
              <input type="radio" name="yes_no" disabled />
              <label> Intersex/Unspecified</label>
            </div>
            <br />
          </div>

          <div className="questionContainer-1">
            <li>Employment Status:</li>
            <select name="Employment Status:" disabled>
              <option value="Entrepreneur">Entrepreneur</option>
              <option value="Full Time">Full Time</option>
              <option value="Part Time">Part Time</option>
            </select>
          </div>

          <div className="questionContainer-1">
            <li>Age Group:</li>
            <select name="Employment Status:" disabled>
              <option value="18-24">18-24</option>
              <option value="25-30">25-30</option>
              <option value="30-40">30-40</option>
              <option value="Over 40">>Over 40</option>
            </select>
          </div>

          <div className="questionContainer-1">
            <li>Are you voting to our party?</li>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> YES</label>
            </div>
            <br />
            <div style={{ opacity: 0.5 }}>
              <input type="radio" name="yes_no" disabled />
              <label> NO</label>
            </div>
          </div>

          <div className="questionContainer-1">
            <li>What's your policy preference?</li>
            <select name="What's your policy preference?" disabled>
              <option value="Education">Education</option>
              <option value="Tax cut">Tax cut</option>
              <option value="Pay rise">Pay rise</option>
            </select>
          </div>
        </ol>
      </form>
    </div>
  );
};
export default DefaultQuestions;
