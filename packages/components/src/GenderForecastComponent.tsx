
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import PieChartComponent from './pieChartComponentForGender'
interface GenderForecastComponentProps{
    data:any,
}
function toPercent2(point: any) {
    var str = Number(point * 100).toFixed(2);
    str += "%";
    return str;
}
const extractCategory = (data:any, category:string)=>{
    return data.filter((item:any)=>{
        if(item.gender === category){
            return 1
        }else{
            return 0
        }
    }).length
}
const extractPositive = (data:any)=>{
    return data.filter((item:any)=>{
        if(item.ifPositive === 1){
            return 1
        }else{
            return 0
        }
    })
}
const GenderForecastComponent: React.FC<GenderForecastComponentProps> = (props) => {
    const {data} = props
    const female = extractCategory(data,'female')
    const male = extractCategory(data,'male')
    const unknown  = data.length - female - male
    const sum = data.length
    const positiveData = extractPositive(data)
    const female2 = extractCategory(positiveData,'female')
    const male2 = extractCategory(positiveData,'male')
    const unknown2  = positiveData.length - female2 - male2
    const sum2 = positiveData.length
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '100%',
            height: '100%',
            // display: 'flex',
            // flexFlow: 'row wrap',
            // alignContent: 'flex-start'
        },
        overViewHeader: {
            marginLeft: 10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            // flex: '0 0 19%',
            fontWeight: 700,
        },
        childContainer: {
            marginTop: '20px',
            marginLeft: 'auto',
            marginRight: 'auto',
            height:'80%',
            width: '90%',
            display: 'flex',
            flexFlow: 'row wrap',
            // alignContent: 'flex-start'
        },
        child1: {
            flex: '0 0 25%'
        },
        child2: {
            flex: '0 0 25%',
            display: 'flex',
            flexFlow: 'column wrap',
        },
        label: {
            flex: '0 0 20%',
        },
        dot: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#A3A1FB',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        dot2: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#5FE3A1',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        dot3: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#56D9FE',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        text: {
            marginTop: '10px',
            marginLeft: '10px',
            display: 'inline-block',
            color: '#232425',
            fontSize: 20,
        },
        labelText: {
            color: '#232425',
            opacity: 0.5,
            fontSize: 10,
            marginLeft: '20px',
        },
        line:{
            flex:'0 0 5%',
            marginRight:'190px',
            marginLeft: '20px',
        },
    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <Typography className={classes.overViewHeader}>
                Campaign Forecast by Gender
            </Typography>
            <div className={classes.childContainer}>
                <div className={classes.child1}>
                    <PieChartComponent title={'Total Positive Voters'} female={female2} male={male2} sum={sum} unspecify={unknown2} />
                </div>
                <div className={classes.child2}>
                    <div className={classes.label}>
                        <div className={classes.dot}></div>
                        <Typography className={classes.text}>{sum2?toPercent2(female2/sum2):'0%'}</Typography>
                        <Typography className={classes.labelText}>Female</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot2}></div>
                        <Typography className={classes.text}>{sum2?toPercent2(unknown2/sum2):'0%'}</Typography>
                        <Typography className={classes.labelText}>Intersex / Unspecified</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot3}></div>
                        <Typography className={classes.text}>{sum2?toPercent2(male2/sum2):'0%'}</Typography>
                        <Typography className={classes.labelText}>Male</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                </div>
                <div className={classes.child1}>
                    <PieChartComponent title={'Total Voters'} female={female} male={male} sum={sum2} unspecify={unknown} />
                </div>
                <div className={classes.child2}>
                <div className={classes.label}>
                        <div className={classes.dot}></div>
                        <Typography className={classes.text}>{sum?toPercent2(female/sum):'0%'}</Typography>
                        <Typography className={classes.labelText}>Female</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot2}></div>
                        <Typography className={classes.text}>{sum?toPercent2(unknown/sum):'0%'}</Typography>
                        <Typography className={classes.labelText}>Intersex / Unspecified</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot3}></div>
                        <Typography className={classes.text}>{sum?toPercent2(male/sum):'0%'}</Typography>
                        <Typography className={classes.labelText}>Male</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default GenderForecastComponent