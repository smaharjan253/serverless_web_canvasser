import React from "react"
import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import { useHistory } from 'react-router-dom';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import barchart1 from './BarChart.svg'
import barchart2 from './BarChart-2.svg'
import barchart3 from './BarChart-3.svg'
import barchart4 from './BarChart-4.svg'

interface OverViewComponentProps {
    data: any;

}
interface dataInterface {
    [key:string]: number;

}

const barcharts = [barchart1,barchart2,barchart3,barchart4]
const OverViewComponent: React.FC<OverViewComponentProps> = (props) => {
    function unique (arr:any) {
        return Array.from(new Set(arr))
      }
    const { data } = props
    const records = data.records
    console.log(records)
    const processData = (dataLoad:any)=>{
        const volunteersNumber = unique(dataLoad.map((item:any)=>{
            return item.volunteerId
        })).length;
        const totalVisits = dataLoad.length;
        const successfulVisits = dataLoad.filter((item:any)=>{
            if(item.successful>0){
                return 1
            }else{
                return 0
            }
        }).length;
        const positiveVisits = dataLoad.filter((item:any)=>{
                if(item.ifPositive){
                    return 1
                }else{
                    return 0
                }
            }).length
        return {
            'VOLUNTEERS': volunteersNumber,
            'TOTAL VISITS': totalVisits,
            'SUCCESSFUL VISITS': successfulVisits,
            'POSITIVE VOTERS': positiveVisits,
        }
    }
    const datas:dataInterface = processData(records)
    const useStyles = makeStyles(theme => ({
        fourBoxes: {
            marginTop:theme.spacing(1),
        },
        boxItemLeft:{
            marginLeft:0,
            padding:theme.spacing(1),
            marginTop:theme.spacing(1),
            marginRight:theme.spacing(1),
            marginBottom:theme.spacing(1),
            // padding:theme.spacing(1),
            backgroundColor: '#fff',
            width:'48%',
            float:'left'
        },
        boxItemRight:{
            marginRight:0,
            padding:theme.spacing(1),
            marginTop:theme.spacing(1),
            marginLeft:theme.spacing(1),
            marginBottom:theme.spacing(1),
            // padding:theme.spacing(1),
            backgroundColor: '#fff',
            width:'48%',
            float:'right'
        },
        boxTopText:{
            // width:'40%',
            // float:'left'
            fontSize: '20px',
            fontFamily:'Segoe UI',
            color:'#4D4F5C'
        },
        boxBottomText:{
            width:'80%',
            float:'left',
            color:'#43425D',
            fontSize: '14px',
            fontFamily:'Segoe UI',
            letterSpacing:'1.5px',
            opacity:0.5,
        },
        boxBottomIcon:{
            // width:'40%',
            float:'right',
            paddingTop:2
        },
        overViewHeader:{
            fontSize: 20,
            fontFamily:'Segoe UI',
            color:'#0A437C',
        }
        
    }));

    
    const classes = useStyles();
    return (
        <Box>
            <Typography className={classes.overViewHeader}>
                Overview
            </Typography>
            <Box className={classes.fourBoxes}>
                {Object.keys(datas).map((key:string,index:number) => (
                    <Box
                        key={key}
                        className={index%2==0?classes.boxItemLeft:classes.boxItemRight}
                    >
                        <Typography className={classes.boxTopText}>
                            {datas[key]}
                        </Typography>
                        <Typography className={classes.boxBottomText}>
                            {key}
                        </Typography>
                        <Box className={classes.boxBottomIcon}>
                            <img src={barcharts[index]} />
                        </Box>
                    </Box>
                )
                )}
            </Box>
        </Box>
    )

}
export default OverViewComponent