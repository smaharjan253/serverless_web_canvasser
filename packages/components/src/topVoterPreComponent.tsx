import React from "react"
import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box, TableBody,TableCell } from "@material-ui/core"
import { useHistory } from 'react-router-dom';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import TableRow from '@material-ui/core/TableRow';
import MoreButton from './more button.svg'
interface TopVoterProprs {
    data: any;
}
const TopVoterPreComponent: React.FC<TopVoterProprs> = (props) => {
    const { data } = props
    
    const records = data.records
    // console.log(records)
    const recordLength = records.length
    const processData = (records:any)=>{
        let results = new Map<string, any>();
        records.map((item:any)=>{
            if(results.has(item.policyName)){
                const policyName:string = item.policyName
                results.set(policyName,{
                    nums:results.get(policyName).nums+1,
                    state:item.state1,
                    surburb:item.surburb,
                    fund:policyName,
                })
            }else{
                const policyName:string = item.policyName
                results.set(policyName,{
                    nums:1,
                    state:item.state1,
                    surburb:item.surburb,
                    fund:policyName,
                })
            }
        })
        // console.log(results)
        return Array.from(results,x=>{
            return {
                percents:(parseInt(((x[1].nums/recordLength)*100).toString())).toString()+'%',
                state:x[1].state,
                surburb:x[1].surburb,
                fund:x[1].fund,
            }
        })
        // records.map(function (map: any, obj: any) {
        //     if (map.has(obj.policyName)) {
        //         map[obj.policyName] = {
        //             nums: map[obj.policyName].nums + 1,
        //             state: obj.state,
        //             surburb: obj.surburb,
        //             fund:obj.policyName,
        //         }
        //     } else {
        //         map[obj.volunteerId] = { visits: 1, positiveVisits: obj.ifPositive > 0 ? 1 : 0, name: obj.fullname };
        //     }
        //     return map;
        // }, {});
        // return results.map((item:any)=>{
        //     return {
        //         percents:item.nums/recordLength,
        //         state:item.state,
        //         surburb:item.surburb,
        //         fund:item.fund,
        //     }
        // })
        // return [{percents:'75%',state:'state',surburb:'surburb',fund:'fund'}]
    }
    // const [data, setData] = useState({ title: 'account info', dataList: [] });
    const data1 = processData(records)

    const colors = [
        '#5CDAFE',
        '#C4C3FC',
        '#FFBE05',
        '#65E2A5',
        '#FF5660',
        '#4880FF',
    ]
    const history = useHistory()
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            padding:theme.spacing(1),
            backgroundColor: '#ffffff',
            height: '90%',
            width: '100%',
            display:'flex',
            flexDirection:'column'
        },
        headBox:{
            width:'100%',
            display:'flex',
            flexDirection:'row',
            flex:'0.1'
        },
        boxTopText: {
            flex:'0.9',
            fontSize: 20,
            // textAlign:'center',
            // paddingTop:5,
            margin:'auto',
            // fontSize: 20,
            color: '#0A437C',
            fontFamily: 'Segoe UI'
            // lineHeight:'30px',
            // height:'30px',
        },
        moreBox: {
            fontSize: 15,
            flex:'0.1',
            display:'flex',
            marginLeft:'auto'
        },
        moreText: {
            flex:'1',
            textAlign: 'right'
        },
        moreIcon: {
            flex:'0.5',
        },
        tableContainer:{
            marginTop:theme.spacing(-1),
            flex:'0.9'
        },
        tableRow:{
            minHeight:10,
            height:20,
        },
        tableRow1:{
            minHeight:10,
            height:20,
            // padding:10,
            borderBottom:0,
            paddingTop:'20px',
            // display:'flex',
            // alignItems: 'center',
            // lineHeight:20,
        },
        tableRow2:{
            minHeight:10,
            height:20,
            padding:10,
            borderBottom:0,
            fontSize:16,
            fontFamily:'Segoe UI',
            color:'#4A4A4A',
            opacity:0.3,
        },
        tableRow3:{
            minHeight:10,
            height:20,
            padding:10,
            borderBottom:0,
            fontSize:16,
            fontFamily:'Segoe UI',
            color:'#4A4A4A',
        },
        tableRow4:{
            minHeight:10,
            height:20,
            padding:10,
            borderBottom:0,
            fontSize:16,
            fontFamily:'Segoe UI',
            color:'#4A4A4A',
        },
        tableRow5:{
            minHeight:10,
            height:20,
            padding:10,
            borderBottom:0,
            fontSize:16,
            fontFamily:'Segoe UI',
            color:'#4A4A4A',
        },
        moreBoxIcon:{
            float:'right'
        }
    }));


    const classes = useStyles();

    return (
        <Box className={classes.wholePanel}>
            <CssBaseline />
            <Box className={classes.headBox}>
                <Typography className={classes.boxTopText}>
                    Top Voter's Policy Preference
                </Typography>
                <Box className={classes.moreBox} >
                    <MoreButton className={classes.moreBoxIcon}/>
                </Box>
            </Box>
            <TableContainer className={classes.tableContainer}>
                <Table>
                    <TableBody>
                        {data1.map((item,index)=>{
                            return (
                                <TableRow 
                                // key={item.policyName}
                                className={classes.tableRow}>
                                    <TableCell className={classes.tableRow1}><RadioButtonUncheckedIcon style={{ color: colors[index]}}/></TableCell>
                                    <TableCell className={classes.tableRow2}>{item.percents}</TableCell>
                                    <TableCell className={classes.tableRow3}>{item.fund}</TableCell>
                                    <TableCell className={classes.tableRow4}>{item.state}</TableCell>
                                    <TableCell className={classes.tableRow5}>{item.surburb}</TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    )
}

export default TopVoterPreComponent
