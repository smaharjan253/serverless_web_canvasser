
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
interface smallBoxProps{
    title:string,
    percent:string,
}
const SmallBox: React.FC<smallBoxProps> = (props)=>{
    
    const { title,percent } = props
    const useCurrentStyles = makeStyles(theme => ({
        wholePanel: {
            width: '90%',
            height: '100%',
            display: 'flex',
            flexFlow: 'row wrap',
            flex:'0 0 10%',
            marginLeft:'auto',
            marginRight:'auto',
        },
        textLeft:{
            flex:'0 0 15%',
            marginTop:'auto',
            marginBottom:'auto',
        },
        textRight:{
            flex:'0 0 10%',
            marginLeft:'auto',
            marginTop:'auto',
            marginBottom:'auto',
        },
        bar:{
            flex:'0 0 70%',
            backgroundColor:'#F0F2F8',
            height:'10px',
            marginTop:'auto',
            marginBottom:'auto',
        },
        progressbar:{
            width:percent,
            height:'10px',
            backgroundColor:'#8BD6FE',
        }

    }));
    const classes = useCurrentStyles();
    return (
        <div className={classes.wholePanel}>
            <Typography className={classes.textLeft}>
                {title}
            </Typography>
            <div className={classes.bar}>
                <div className={classes.progressbar}></div>
            </div>
            <Typography className={classes.textRight}>
                {percent}
            </Typography>
        </div>
    )
}
interface EmploymentComponentProps{
    data:any,
}
const EmploymentComponent: React.FC<EmploymentComponentProps> = (props) => {
    const {data} = props
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '100%',
            height: '100%',
            display: 'flex',
            flexFlow: 'column wrap',
        },
        overViewHeader:{
            marginLeft:10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            flex: '0 0 30%',
            fontWeight:700,
        }
    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <Typography className={classes.overViewHeader}>
                Positive Voter’s Employment Status
            </Typography>
            {
                data.map((item:any)=>{
                    return (<SmallBox title={item.title} percent={item.percent} />)
                })
            }
        </div>
    )
}

export default EmploymentComponent