import makeStyles from "@material-ui/core/styles/makeStyles"
import { Theme } from "@material-ui/core"

const drawerWidth = 240

const drawerStyles = makeStyles((theme: Theme) => ({
    root: {
        display: "flex",
    },
    appBar: {
        background: "white",
        color: "black",
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    title: {
        flexGrow: 1,
        alignSelf: 'center',
        fontStyle: 'italic',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: "nowrap",
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerClose: {
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        overflowX: "hidden",
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9) + 1
        }
    },
    toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar
    },
    ExitToAppIcon: {
        alignItems: "right"
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3)
    },
    navList: {
        flexGrow: 1,
        color: "#2699FB",
    },
    navListIcon: {
        color: "#2699FB",
    }
}))
export default drawerStyles
