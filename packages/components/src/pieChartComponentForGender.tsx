import React, { PureComponent } from 'react'
import { PieChart, Pie, Sector, Cell } from 'recharts'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
const data = [
    //   [{ name: 'Positive', value: 400 },{name:'others',value:0}],
    //   [{ name: 'Negative', value: 300 },{name:'others',value:0}],
    //   [{ name: 'left', value: 300 },{name:'others',value:0}],
    { name: 'Positive', value: 400 },
    { name: 'Negative', value: 300 },
    { name: 'left', value: 300 }
];

const renderActiveShape = (props: any) => {
    const RADIAN = Math.PI / 180;
    const {
        cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
        fill, payload, percent, value,
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} dy={1} textAnchor="middle" fill={fill}>{payload.name}</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`PV ${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
                {`(Rate ${(percent * 100).toFixed(2)}%)`}
            </text>
        </g>
    );
};
const innerRadius = [40, 50, 60]
const outerRadius = [90, 80, 70]
const cx = 110;
const cy = 100;
const COLORS = ['#0088FE', '#00C49F', '#FFBB28'];
interface PieComponentProps {
    title: string,
    // successfulVisits: string;
    female: number,
    male: number,
    unspecify: number,
    sum: number,
}
class PieChartComponent extends PureComponent<PieComponentProps> {
    // constructor(props:any) {
    //     super(props)
    //     this.state = {
    //         suceessNumber : props.successfulVisits
    //     }
    // }
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/hqnrgxpj/';








    onPieEnter = (data: any, index: any) => {
        this.setState({
            activeIndex: index,
        });
    };

    render() {
        return (
            <PieChart width={250} height={200} style={{
                // filter:url(#shadow)
                // filter:"drop-shadow(0px 0px 4px #65E2A5)",
            }}>
                {/* <filter id="blurMe">
                    {/* <feGaussianBlur stdDeviation="1"  */}
                {/* <feGaussianBlur result="blurOut" in="offOut" stdDeviation="6" />
                    <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
                    />
                </filter> */} */}
                <g >

                    <text
                        fontSize={30}
                        fill="#232425"
                        x={cx} y={cy - 10} dy={10} textAnchor="middle" >{this.props.sum}</text>
                    <text
                        fontSize={10}
                        fill="#232425"
                        opacity={0.5}
                        x={cx} y={cy + 10} dy={10} textAnchor="middle" >{this.props.title}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={70}
                        outerRadius={98}
                        endAngle={90 - this.props.female * 360 / this.props.sum}
                        startAngle={90}
                        fill="#A3A1FB"
                    // filter="url(#blurMe)"
                    />
                    {/* <Sector
                    cx={cx}
                    cy={cy}
                    style={{
                        boxShadow: "30px",
                        // opacity:0.2,
                        // borderRadius: "8px"
                    }}
                    innerRadius={90}
                    outerRadius={95}
                    endAngle={90 - data[0].value * 360 / 1000}
                    startAngle={90}
                    fill="#C1FFC1"
                    // className={classes.shadow}
                /> */}
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={70}
                        outerRadius={98}
                        startAngle={90 - this.props.female * 360 / this.props.sum}
                        endAngle={90 - this.props.female * 360 / this.props.sum - this.props.male * 360 / this.props.sum}
                        fill="#56D9FE"
                    />
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={70}
                        outerRadius={98}
                        startAngle={90 - this.props.female * 360 / this.props.sum - this.props.male * 360 / this.props.sum}
                        endAngle={-270}
                        fill="#5FE3A1"
                    />
                </g>
            </PieChart >
        );
    }
}

export default PieChartComponent