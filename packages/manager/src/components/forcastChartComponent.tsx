import React from "react"
import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import PieChartComponent from './pieChartComponent'
import MoreButton from './more button.svg'
interface OverViewComponentProps {
    data: any;

}
function unique(arr: any) {
    return Array.from(new Set(arr))
}
interface shwonDataProps {
    positiveVisits: number;
    negativeVisits: number;
    successfulVisits: string;
    sum:number;
}
const ForcastChartComponent: React.FC<OverViewComponentProps> = (props) => {
    const [selection, setSelection] = React.useState('');
    // const initialData: any[] = []
    // const [selectedData, setSelectedData] = React.useState(initialData);
    const { data } = props
    const records = data.records

    const processData = (dataLoad: any) => {
        const positiveVisits:number = dataLoad.filter((item: any) => {
            if (item.ifPositive) {
                return 1
            }
        }).length
        const successfulVisits:number = dataLoad.filter((item: any) => {
            if (item.successful) {
                return 1
            }
        }).length
        const negativeVisits:number = successfulVisits - positiveVisits;
        const successfulVisitss:string = (parseInt((successfulVisits*100/dataLoad.length).toString())).toString()+'%'
        const menuItems: any[] = unique(dataLoad.map((item: any) => {
            return item.surburb
        }))
        return {
            shownData: {
                positiveVisits: positiveVisits,
                negativeVisits: negativeVisits,
                successfulVisits: successfulVisitss,
                sum:dataLoad.length,
            },
            menuItems: menuItems
        }
    }

    let selectedData = []
    if (selection != '') {
        selectedData = records.filter((item: any) => {
            return item.surburb === selection ? 1 : 0
        })
    } else {
        selectedData = records
    }
    const result1 = processData(selectedData)
    const result2 = processData(records)
    const shownData: shwonDataProps = result1.shownData
    const menuItems: any[] = result2.menuItems
    const useStyles = makeStyles(theme => ({
        wholeContainer: {
            marginTop: theme.spacing(1),
            height: '100%',
            backgroundColor: '#fff',
            padding: theme.spacing(1),
        },
        leftContainer: {
            width: '50%',
            height: '100%',
            float: 'left',
        },
        boxTopText: {
            width: '100%',
            float: 'left',
            fontSize: 20,
            color: '#0A437C',
            fontFamily: 'Segoe UI'
        },
        moreBox: {
            marginRight: 0,
            fontSize: 15,
            float: 'right',
        },
        moreText: {
            float: 'right',
            textAlign: 'right'
        },
        moreIcon: {
            float: 'right',
        },
        chart: {
            float: 'right',
            marginTop: theme.spacing(2),

            marginRight: theme.spacing(5),
        },
        secondLine: {
            clear: 'both',
            position: 'absolute',
            // marginLeft:theme.spacing(1),
            float: 'left'
        },
        formControl: {
            marginTop: theme.spacing(2),
            margin: theme.spacing(1),
            // marginLeft:theme.spacing(1),
            minWidth: 120,
            clear: 'both',
            float: 'left'
        },
        dropdowntext: {
            marginTop: 0,
            // float:'left',
            fontSize: 10,
            // position:'absolute'
        },
        dropdownbox: {
            // height: 30,
            // position: 'relative',
            marginTop:5,
            paddingTop:0,
            paddingBottom:0,
        },
        middleLabel: {
            marginTop: theme.spacing(3),
            marginLeft: theme.spacing(6),
            float: 'right',
        },
        labelBox: {
            height: 5,
            width: 25,
            borderRadius: 2,
            float: 'left',
            backgroundColor: '#0000CD'
        },
        labelBox1: {
            marginTop: theme.spacing(1),
            height: 5,
            width: 25,
            borderRadius: 2,
            float: 'left',
            backgroundColor: '#00EE76'
        },
        labelText1: {
            clear: 'both',
            fontSize: 10,
        },
        labelText2: {
            fontSize: 8
        }

    }));

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setSelection(event.target.value === 'All'?'': event.target.value as string);
    }



    const classes = useStyles();
    return (
        <Box className={classes.wholeContainer}>
            <Box className={classes.leftContainer}>
                <Typography className={classes.boxTopText} >
                    Campaign forecast
                </Typography>
                <FormControl
                    // margin='dense'
                    variant="outlined"
                    className={classes.formControl}>
                    <InputLabel
                        // shrink={true}
                        // margin='dense' 
                        // disableAnimation={true}
                        // id="demo-simple-select-label"
                        className={classes.dropdowntext}
                        >
                        PostCode
                    </InputLabel>
                    <Select
                        className={classes.dropdownbox}

                        // labelId="demo-simple-select-outlined-label"
                        // id="demo-simple-select-outlined"
                        value={selection===''?'All':selection}
                        onChange={handleChange}
                    // labelWidth={labelWidth}
                    >
                        <MenuItem value="All">
                            <em>All</em>
                        </MenuItem>
                        {menuItems.map((item) => {
                        return <MenuItem value={item}>{item}</MenuItem>
                        })}
                    </Select>
                </FormControl>
                
                <Box className={classes.middleLabel}>
                    <Box>
                        <Box className={classes.labelBox}>

                        </Box>
                        <Typography className={classes.labelText1}>
                            Positive Voters
                        </Typography>
                        <Typography className={classes.labelText2}>
                            {shownData.positiveVisits}
                        </Typography>
                    </Box>
                    <Box>
                        <Box className={classes.labelBox1}>

                        </Box>
                        <Typography className={classes.labelText1}>
                            Negative Voters
                        </Typography>
                        <Typography className={classes.labelText2}>
                            {shownData.negativeVisits}
                        </Typography>
                    </Box>
                </Box>
            </Box>
            <Box className={classes.moreBox} >
                <MoreButton />
            </Box>
            <Box className={classes.chart}>
                <PieChartComponent 
                successfulVisits={shownData.successfulVisits} 
                positiveVisits={shownData.positiveVisits}
                negativeVisits={shownData.negativeVisits}
                sum={shownData.sum}/>
            </Box>

        </Box>
    )

}
export default ForcastChartComponent