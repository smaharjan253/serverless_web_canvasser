import React from 'react';
import clsx from 'clsx';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles';
import { Button, Container, CssBaseline, Divider, Box, Drawer } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { callbackify } from 'util';

interface Data {
  calories: number;
  carbs: number;
  fat: number;
  name: string;
  protein: number;
  action: string;
}

function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number,
  action: string
) {
  return { name, calories, fat, carbs, protein, action };
}
const tableTitle = 'Manage Users'
const rows = [
  createData('Cupcake', 305, 3.7, 67, 4.3, ''),
  createData('Donut', 452, 25.0, 51, 4.9, ''),
  createData('Eclair', 262, 16.0, 24, 6.0, ''),
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0, ''),
  createData('Gingerbread', 356, 16.0, 49, 3.9, ''),
  createData('Honeycomb', 408, 3.2, 87, 6.5, ''),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3, ''),
  createData('Jelly Bean', 375, 0.0, 94, 0.0, ''),
  createData('KitKat', 518, 26.0, 65, 7.0, ''),
  createData('Lollipop', 392, 0.2, 98, 0.0, ''),
  createData('Marshmallow', 318, 0, 81, 2.0, ''),
  createData('Nougat', 360, 19.0, 9, 37.0, ''),
  createData('Oreo', 437, 18.0, 63, 4.0, ''),
];

const rows2 = [
  ['Cupcake', 305, 3.7, 67, 4.3, '@'],
  ['Donut', 452, 25.0, 51, 4.9, '@'],
  ['Eclair', 262, 16.0, 24, 6.0, '@'],
  ['Gingerbread', 356, 16.0, 49, 3.9, '@'],
  ['Honeycomb', 408, 3.2, 87, 6.5, '@'],
  ['Ice cream sandwich', 237, 9.0, 37, 4.3, '@'],
  ['Jelly Bean', 375, 0.0, 94, 0.0, '@'],
  ['KitKat', 518, 26.0, 65, 7.0, '@'],
  ['Lollipop', 392, 0.2, 98, 0.0, '@'],
  ['Marshmallow', 318, 0, 81, 2.0, '@'],
  ['Nougat', 360, 19.0, 9, 37.0, '@'],
  ['Oreo', 437, 18.0, 63, 4.0, '@']
]

function desc<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort<T>(array: T[], cmp: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function sortByKey(array: any, order: string, orderBy: number) {
  // console.log(order)
  // console.log(orderBy)
  array.sort((a: any, b: any) => {
    const bigger = a[orderBy] > b[orderBy];
    const result = order === 'asc' ? bigger ? 1 : -1 : bigger ? -1 : 1;
    return result
  });
  // console.log(array)
  return array
}

type Order = 'asc' | 'desc';

function getSorting<K extends keyof any>(
  order: Order,
  orderBy: K,
): (a: { [key in K]: number | string }, b: { [key in K]: number | string }) => number {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

interface HeadCell {
  disablePadding: boolean;
  id: number;
  label: string;
  numeric: boolean;
  needsort: boolean;
}

const headCells: HeadCell[] = [
  { id: 0, numeric: false, disablePadding: true, label: 'USERNAME', needsort: true },
  { id: 1, numeric: true, disablePadding: false, label: 'FULLNAME', needsort: true },
  { id: 2, numeric: true, disablePadding: false, label: 'AGE', needsort: false },
  { id: 3, numeric: true, disablePadding: false, label: 'SURBURB', needsort: false },
  { id: 4, numeric: true, disablePadding: false, label: 'CONTACT', needsort: false },
  { id: 5, numeric: true, disablePadding: false, label: 'EMAIL', needsort: false },
  { id: 6, numeric: true, disablePadding: false, label: 'ACTION', needsort: false },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: number) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
  order: Order;
  orderBy: number;
  rowCount: number;
  header: Array<any>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, header } = props;
  const createSortHandler = (property: number) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };
  const createEmptyHandler = (property: number) => (event: React.MouseEvent<unknown>) => {
    // onRequestSort(event, property);
  };

  return (
    <TableHead className={classes.tableHead}>
      <TableRow >
        {/* <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell> */}
        {header.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'center'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
            className={classes.tableFirstRow}
          >
            <TableSortLabel
              className={classes.tableHeadFont}
              hideSortIcon={headCell.needsort ? false : true}
              //   active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={headCell.needsort ? createSortHandler(headCell.id) : createEmptyHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: 0,
      paddingRight: 0,
    },
    highlight:
      theme.palette.type === 'light'
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
    title: {
      flex: '1 1 100%',
      fontSize: 20,
      color: '#0A437C',
      fontFamily: 'Segoe UI'
    },
    addButtonTitle: {
      float: 'left',
      // paddingTop:theme.spacing(1),
      // position: 'absolute',
      paddingLeft: 0,
      paddingTop: 13,
      fontWeight: 'bold',

    },
    addButton: {
      float: 'left',
      width: '10%',
      paddingRight: 0,
    }
  }),
);

interface EnhancedTableToolbarProps {
  numSelected: number;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <Typography className={classes.title} variant="h6" id="tableTitle">
        {tableTitle}
      </Typography>


      <Box>
        <Typography className={classes.addButtonTitle}>Add New</Typography>
        <IconButton aria-label="Add" className={classes.addButton}>
          <AddBoxOutlinedIcon style={{ color: '#000' }} />
        </IconButton>
      </Box>

    </Toolbar>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 400,
      // float:'left'
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    tableContainer: {
      maxHeight: 500,
      // display:'table',
      width: 'fit-content'
    },
    tableFirstRow: {
      borderBottomWidth: 0,
      borderBottom: 'none'
      // elevation:0
    },
    tableHead: {
      backgroundColor: '#F5F6FA'
    },
    tableHeadFont: {
      fontFamily: 'Segoe UI',
      fontWeight: 'bold',
      color: '#7B7C81',
      fontSize: 10
    },
    item: {
      color: '#4A4A4A',
      fontFamily: 'Segoe UI',
      fontSize: '15px',
      // width:'fit-content',
      // minWidth:50
    }
  }),
);
interface EnhancedTableAllProps {
  data: Array<any>;
  headers: Array<string>
  editCallback: any;
  deleteCallback: (id:string)=>void;
}
export default function EnhancedTable(props: EnhancedTableAllProps) {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<number>(1);
  const [selected, setSelected] = React.useState<string[]>([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(rows.length > 30 ? 30 : rows.length);

  //added
  const { data, headers ,editCallback,deleteCallback} = props

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: number) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };

  const handleEdit = (event: React.MouseEvent<unknown>, id: string) => {
    //jump to edit page
    const dataForEdit = data.filter((item)=>{
      return item[0] === id
    });
    editCallback(dataForEdit);
  }

  const handleDelete = (event: React.MouseEvent<unknown>, id: string) => {
    //sendrequest to delete data and pull new data
    deleteCallback(id)
  }

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };



  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      {/* <Paper className={classes.paper}> */}
      <EnhancedTableToolbar numSelected={0} />
      <TableContainer className={classes.tableContainer}>
        <Table
          // stickyHeader
          className={classes.table}
          aria-labelledby="tableTitle"
          size={dense ? 'small' : 'medium'}
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            numSelected={selected.length}
            order={order}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
            header={headers}
          />
          <TableBody>
            {sortByKey(data, order, orderBy)
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: any, index: any) => {
                const isItemSelected = isSelected(row.name);
                const labelId = `enhanced-table-checkbox-${index}`;
                // console.log(row[0])
                return (
                  <TableRow
                    hover
                    // onClick={event => handleClick(event, row[0])}
                    //   role="checkbox"
                    //   aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row[0]}
                  //   selected={isItemSelected}
                  >
                    <TableCell className={classes.item} align="center" component="th" id={labelId} scope="row" padding="none" >
                      {row[0]}
                    </TableCell>
                    {row.map((item: any, index: number) => {
                      if (index == 0) return
                      return (<TableCell className={classes.item} key={item + index} align="right">{row[index]}</TableCell>)
                    })}

                    <TableCell align="right">
                      <EditIcon
                        onClick={event => handleEdit(event, row[0])}
                      />
                      <DeleteIcon
                        onClick={event => handleDelete(event, row[0])}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[30]}
        component="div"
        count={rows.length}
        rowsPerPage={30}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}

      />
      {/* </Paper> */}
    </div>
  );
}
