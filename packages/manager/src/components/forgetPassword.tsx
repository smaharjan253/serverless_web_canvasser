export default ''
// import React from "react"
// import useStyles from './utils/styles'
// import ValidationTextField from './utils/ValidationTextField'
// import { useForm } from './utils/useForm'
// import { CssBaseline, Container, Button, Divider, Typography } from "@material-ui/core"
// import { useHistory } from 'react-router-dom';
// import constant from './constant';
// import sendemail  from './utils/sendEmail';

// const ForgetPassword: React.FC = () => {
//     const classes = useStyles()
//     const history = useHistory()

//     const { values, useInput, isValid } = useForm({
//         email: ''
//     })
//     const getAccountInfo = (values: any) => {
//         // console.log('enter111')
//         const callback = ''
//         const email = values.email
//         const url = constant.forgotPasswordUrl
//         fetch(url, {
//             method: 'POST',
//             headers: { 'Accept': 'application/json'},
//             body: JSON.stringify({
//                 email: email,
//             }),
//         })
//             .then(function (response) {
//                 return response.json()
//             })
//             .then(function (myjson) {
//                 console.log(myjson);
//                 if(myjson.length>0){
//                     sendemail(myjson,null)
//                 }
//             })
//             .catch(error => console.error('Error:', error));;
//     }
//     const handleSubmit = (event: any) => {
//         event.preventDefault()
//         console.log(values)
//         history.push("/url")
//         //send request to get account information
//         getAccountInfo(values)
//     }

//     return (
//         <div>
//             <CssBaseline />
//             <div className={classes.banner}>
//                 <label className={classes.bannerText}>Web Canvasser</label>
//             </div>

//             <Container component="main" maxWidth="xs">
//                 <div className={classes.paper}>
//                     <form className={classes.form}>

//                         <Typography className={classes.typography}>Get Back Your Login Credentials</Typography>

//                         <ValidationTextField
//                             variant="outlined"
//                             margin="normal"
//                             fullWidth
//                             id="email"
//                             label="Email Address"
//                             name="email"
//                             autoComplete="email"
//                             autoFocus
//                             value={values.email}
//                             className={classes.textField}
//                             {...useInput('email', 'isRequired, isEmail')} />
//                     </form>

//                     <Divider component="ul" className={classes.divider} />

//                     <Button
//                         type="submit"
//                         disabled={!isValid}
//                         fullWidth
//                         variant="contained"
//                         onClick={handleSubmit}
//                         className={classes.sub_button}>
//                         Submit
//                     </Button>
//                 </div>
//             </Container>
//         </div>
//     )
// }

// export default ForgetPassword
