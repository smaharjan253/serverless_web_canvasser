import React from 'react';
import { Button } from '@material-ui/core';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import DashboardCommonComponent from './dashboardCommonComponent';
import constant from './constant';
import { useHistory } from 'react-router-dom';
import stack from '@canvasser/components/src/stack.json';

const Dashboard = () => {
	const history = useHistory();
	const [ data, setData ] = useState({ records: [], accounts: [], state: false });

	const requestAccount = (dataload1: any) => {
		const url = constant.getAllAccountBySurburbsUrl;
		const callback = (dataLoad1: any, dataLoad2: any) => {
			setData({ records: dataLoad1, accounts: dataLoad2, state: true });
		};
		const suburbs = [ '1' ];
		fetch(url, {
			method: 'POST',
			headers: { Accept: 'application/json', 'x-api-key': stack.apikey },
			body: JSON.stringify({
				suburbs: suburbs
			})
		})
			.then(function(response) {
				return response.json();
			})
			.then(function(myjson) {
				console.log(myjson);
				console.log(dataload1);
				// if(myjson.length>0){
				//     sendemail(myjson,null)
				// }
				callback(dataload1, myjson);
			})
			.catch((error) => console.error('Error:', error));
	};

	const requestData = () => {
		const url = constant.requestRecordsUrl;
		// const callback = (dataLoad:any)=>{
		//     setData(dataLoad)
		// }
		const suburbs = [ 'suburb', 'suburb2' ];
		fetch(url, {
			method: 'POST',
			headers: { Accept: 'application/json', 'x-api-key': stack.apikey },
			body: JSON.stringify({
				suburbs: suburbs
			})
		})
			.then(function(response) {
				return response.json();
			})
			.then(function(myjson) {
				// console.log(myjson);
				requestAccount(myjson);
				// if(myjson.length>0){
				//     sendemail(myjson,null)
				// }
				// callback(myjson)
			})
			.catch((error) => console.error('Error:', error));
	};

	const editCallback = () => {
		history.push('/AddVolunteer');
	};

	const deleteCallBack = () => {};

	if (data.state === false) {
		requestData();
	}

	const useStyles = makeStyles((theme) => ({
		wholePanel: {
			marginLeft: 69,
			marginTop: 64,
			width:'175vh',
			height: '115vh'
		},
		childComponentContainer: {
			width: '100%',
			height: '100%'
		}
	}));
	const classes = useStyles();

	return (
		<div className={classes.wholePanel}>
			<div className={classes.childComponentContainer}>
				<DashboardCommonComponent editCallback={editCallback} deleteCallback={deleteCallBack} data={data} />
			</div>
		</div>
	);
};

export default Dashboard;
