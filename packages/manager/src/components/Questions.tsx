import React from 'react'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from './component-styles/NavBarStyle'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import ListIcon from '@material-ui/icons/List';

const Questions = () => {
    const classes = useStyles();

    return(
        <div className="container1">
            <div className={classes.questionOptionButton}>
            <Button 
                    component={Link} 
                    to="/DefaultQuestions"
                    color="primary"
                    variant="contained"
                    size="large"
                    style={{width:"60%"}}
                >
                    Default Questions
                </Button>
            <br/>
            <br/>
                <Button 
                    component={Link} 
                    to="/AddQuestions"
                    color="primary"
                    variant="contained"
                    size="large"
                    style={{width:"60%"}}
                >
                    Add Questions
                </Button>
            </div>
        </div>
                )
}
export default Questions