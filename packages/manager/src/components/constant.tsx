import awsmobile from '@canvasser/components/src/aws-exports';
const baseUrl = awsmobile.aws_cloud_logic_custom[0].endpoint
const constant = {
	//parameters for sending email
	templateIdForNew: '3',
	templateIdForForgot: '3',
	serviceId: 'gmail',
	userId: 'user_L9TRRUAYk2vIQ6FGFtaUO',

    //urls
	forgotPasswordUrl: baseUrl + '/getInfoByEmail',
	requestRecordsUrl: baseUrl + '/getRecordsBySurburbs',
	getAllAccountBySurburbsUrl: baseUrl + '/getAllAccountBySurburbs'
};
export default constant;
