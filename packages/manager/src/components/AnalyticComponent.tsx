
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import OverallCampaignForcastComponent from './OverallCampaignForcastComponent'
import EmploymentComponent from './EmploymentComponent'
import GenderForecastComponent from './GenderForecastComponent'
import BarChartComponent from './BarChartComponent'
import LineGraphComponent from './LineGraphComponent'

const AnalyticComponent = () => {
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '1300px',
            height: '700px',
            display: 'flex',
            flexFlow: 'row wrap',
            alignContent: 'flex-start'
        },
        child1: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 47%',
            height: '40%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child2: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 47%',
            height: '40%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child3: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 5%',
        },
        child4: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 99%',
            height: '43%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child5: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 99%',
            height: '51%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        overViewHeader: {
            marginLeft: 10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            fontWeight: 700,
        },
        barchartP: {
            marginTop: '20px'
        }
    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <div className={classes.child5}>
                <LineGraphComponent />
            </div>
            <div className={classes.child1}>
                <OverallCampaignForcastComponent />
            </div>
            <div className={classes.child3}>
            </div>
            <div className={classes.child2}>
                <EmploymentComponent />
            </div>
            <div className={classes.child4}>
                <GenderForecastComponent />
            </div>
            <div className={classes.child1}>
                <Typography className={classes.overViewHeader}>
                    Overall Voters’ Demographics
                </Typography>
                <div className={classes.barchartP}>
                    <BarChartComponent color={'#4880FF'}/>
                </div>
            </div>
            <div className={classes.child3}>
            </div>
            <div className={classes.child2}>
                <Typography className={classes.overViewHeader}>
                    Positive Voters’ Demographics
                </Typography>
                <div className={classes.barchartP}>
                    <BarChartComponent color={'#FFBE05'}/>
                </div>
            </div>
        </div>
    )
}

export default AnalyticComponent