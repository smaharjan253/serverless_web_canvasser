import React from 'react'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import useStyles from './component-styles/NavBarStyle';
import { Box } from '@material-ui/core';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';

const Volunteers = () => {
    const classes = useStyles();

    return(
        <div className="container1">
            <Box className={classes.inComponentButton}>
                <Button 
                    component={Link} 
                    to="/AddVolunteer"
                >
                    Add Volunteer
                    <AddBoxOutlinedIcon />
                </Button>
            </Box>
            <h1 style={{marginLeft:"150px", marginTop:"50px"}}>This is Volunteers</h1>
        </div>
    )
}

export default Volunteers