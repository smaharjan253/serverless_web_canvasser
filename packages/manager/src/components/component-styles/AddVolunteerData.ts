const addVolunteerProps = [
    {
        id: "preferred-location",
        label: 'Preferred Location',
        name: "plocation",
        validation: "isRequired"

    },
    {
        id: "first-name",
        label: 'First Name',
        name: "fname",
        validation: "isRequired"
    },
    {
        id: "full-name",
        label: 'Full Name',
    },
    {
        id: "birth-date",
        label: 'Birth Date',
        name: "birthdate",
        validation: "isRequired"
    },
    {
        id: "Phone-no",
        label: 'Phone No.',
        name: "phone",
        validation: "isRequired"
    },
    {
        id: "email",
        label: 'Email',
        name: "email",
        validation: "isRequired"
    },
    {
        id: "password",
        label: 'Password',
        name: "password",
        validation: {
            isRequired: true,
            isLength: {
                min: 6,
            }
        }
    },
    {
        id: "confirm-password",
        label: 'Confirm Password',
        name: "cpassword",
        validation: {
            isRequired: true,
            isLength: {
                min: 6,
            }
        }
    }
]

export default addVolunteerProps