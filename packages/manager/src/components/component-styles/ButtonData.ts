const buttonProps = [
    {
      id: 1,
      name: 'dashboard',
      path: '/'
    },
    {
      id: 2,
      name: 'volunteers',
      path: '/volunteers'
    },
    {
      id: 3,
      name: 'questions',
      path: '/questions'
    },
    {
      id: 4,
      name: 'analytics',
      path: '/analytics'
    },
  ]

  export default buttonProps