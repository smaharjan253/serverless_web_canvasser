import { makeStyles, Theme, createStyles } from '@material-ui/core'

const drawerWidth = 70;

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        textAlign: 'center',
        width: "100%"
      },
    },
    
    drawer: {

      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
      },
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      background: 'white',
      color: 'black',
      fontFamily: "Helvetica, Arial, sans-serif",
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    inComponentButton: {
      '& > *': {
        margin: theme.spacing(1),
        float: 'right',
        fontFamily: "PT Sans",
        fontWeight: "bold"
      },
    },
    drawerPaper: {
      backgroundColor: '#0A437C',
      color: 'white',
      width: drawerWidth
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    label: {
      textAlign: "center"
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    logout: {
      padding: '0',
      right: '10px',
      display: 'flex',
      position: 'absolute'
    },
    help: {
      margin: "5px",
      bottom: '10px',
      position: 'absolute'
    },
    title: {
      fontFamily: "PT Sans",
      fontSize: '30px',
      fontVariant: "normal", 
      fontWeight: 700
    },
    navButton: {
      top: '160px',
      paddingLeft: "5px",
      paddingBottom: "15px",
      display: 'inline-block'
    },
    content: {
      padding: theme.spacing(3),
    },
    childComponentContainer:{
      width:'100%',
      height:'100%',
    },
    questionOptionButton:{
      width: "35%",
      height: "30%",
      float:"left",
      marginTop:"17%",
      marginLeft:"40%",
    }
}));

export default useStyles