import React, { useState } from 'react';
import { makeStyles, Button, Typography, InputBase, IconButton } from '@material-ui/core';
import { useForm } from '@canvasser/components/src/useForm';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import ValidationTextField from '@canvasser/components/src/ValidationTextField';
import IndeterminateCheckBoxOutlinedIcon from '@material-ui/icons/IndeterminateCheckBoxOutlined';
import generator from 'generate-password';
import awsmobile from '@canvasser/components/src/aws-exports';
import * as data from '@canvasser/components/src/stack.json';
import { API, Auth } from 'aws-amplify';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: theme.spacing(11)
	},
	text: {
		marginLeft: '20px'
	},
	content_div: {
		display: 'flex',
		alignItems: 'left'
	},
	left_div: {
		width: '25%',
		display: 'flex',
		flexDirection: 'column'
		// background: '#ff0000'
	},
	right_div: {
		display: 'flex',
		flexDirection: 'column',
		margin: '0 40px 0',
		width: '25%'
	},
	add_suburb_div: {
		padding: '2px 4px',
		display: 'flex',
		alignItems: 'center',
		border: '2px solid #ACD5F9',
		borderRadius: '5px'
	},
	input: {
		margin: theme.spacing(1),
		flex: 1
	},
	iconButton: {
		padding: 10
	},
	sub_button: {
		background: '#1867AC',
		borderRadius: 5,
		color: 'white', //textcolor
		fontWeight: 'bold',
		height: theme.spacing(6.5),
		padding: '0 30px'
	},
	suburb_list_div: {
		padding: '0px 15px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between'
	}
}));

const AddVolunteer: React.FC = () => {
	const classes = useStyles();
	const history = useHistory()
	const [ box, setBox ] = useState([]);
	const [ name, setName ] = useState('');

	const { values, useInput, isValid } = useForm({
		firstname: '',
		lastname: '',
		email: '',
		birthdate: '',
		contactno: ''
	});

	const addLocation = () => {
		if (box.length == 4) {
			setName('');
			return;
		}
		if (name !== '' && box.length < 4) {
			setBox([
				...box,
				{
					id: box.length,
					value: name
				}
			]);
			setName('');
		}
	};

	const password = generator.generate({
		length: 6,
		numbers: true,
		uppercase: true,
		lowercase: true,
		excludeSimilarCharacters: true,
		strict: true
	});

	const onSubmit = () => {
		onSubmitsignup();
	};

	const onSubmitsignup = () => {
		const usertype = 'Volunteers';
		const username = values.email;
		const password = 'asdfasdf';
		const contact = '+61' + values['contactno'].substring(1);
		const params = {
			given_name: values.firstname,
			family_name: values.lastname,
			birthdate: values.birthdate,
			phone_number: contact,
			'custom:secretcode': password,
			'custom:usertype': usertype,
			'custom:suburb1': box[0]['value'],
			'custom:suburb2': box[1]['value'],
			'custom:suburb3': box[2]['value'],
			'custom:suburb4': box[3]['value']
		};

		try {
			Auth.signUp({ username, password, attributes: params });
			history.push("/Volunteers")
		} catch (error) {
			console.log({ error });
		}
	};

	const onSubmitApi = async () => {
		try {
			const result = await Auth.signIn('a@a.com', 'asdfasdf');
			console.log({ result });
			const token = (await Auth.currentSession()).getIdToken().getJwtToken();
			console.log({ token });

			const params = {
				body: {
					email: 'b@a.com',
					firstname: 'aaa',
					lastname: 'bbb',
					birthdate: '01/01/2015',
					phone_number: '+6112345967809',
					suburbs: [ 'box', 'asd', 'fdg', 'gfa' ]
				},
				headers: {
					Authorization: `Bearer ${token}`
				}
			};

			const res = await API.post(awsmobile.aws_cloud_logic_custom[0].name, '/adduser', params);
			console.log({ res });
		} catch (error) {
			console.log({ error });
		}
	};

	const onSubmitFetch = async () => {
		console.log({ box });

		const params = {
			method: 'POST',
			body: JSON.stringify({
				email: 'a@a.com',
				firstname: 'aaa',
				lastname: 'bbb',
				birthdate: '01/01/2015',
				phone_number: '+6112345967809',
				suburbs: [ 'box', 'asd', 'fdg', 'gfa' ]
				// }
				// body: {
				// 	email: values.email,
				// 	firstname: values.firstname,
				// 	lastname: values.lastname,
				// 	birthdate: values.birthdate,
				// 	phone_number: '+6112345967809',
				// 	suburbs: [ box ]
			}),
			headers: {
				Accept: 'application/json',
				'x-api-key': data.apikey
			}
		};
		console.log({ params });
		const url = awsmobile.aws_cloud_logic_custom[0].endpoint + '/adduser';
		try {
			const resp = await fetch(url, params);
			console.log({ resp });
		} catch (error) {
			console.log({ error });
		}
	};
	return (
		<div className={classes.root}>
			<Typography variant="h4">Add User</Typography>
			<br />

			<div className={classes.content_div}>
				<div className={classes.left_div}>
					<ValidationTextField
						label="First Name"
						variant="outlined"
						name="firstname"
						value={values.firstname}
						{...useInput('firstname', 'isRequired')}
					/>
					<br />
					<ValidationTextField
						label="Last Name"
						variant="outlined"
						name="lastname"
						value={values.lastname}
						{...useInput('lastname', 'isRequired')}
					/>
					<br />
					<ValidationTextField
						label="Birth Date"
						variant="outlined"
						name="birthdate"
						value={values.birthdate}
						{...useInput('birthdate', 'isRequired')}
					/>
					<br />
					<ValidationTextField
						label="Contact No."
						name="contactno"
						variant="outlined"
						value={values.contactno}
						{...useInput('contactno', {
							isRequired: true,
							isLength: {
								min: 10
							}
						})}
					/>
					<br />
					<ValidationTextField
						label="E-mail"
						variant="outlined"
						value={values.email}
						{...useInput('email', 'isRequired, isEmail')}
					/>
					<br />
					<Button
						variant="contained"
						color="primary"
						className={classes.sub_button}
						// disabled={!isValid}
						onClick={() => onSubmit()}
					>
						Submit
					</Button>
				</div>

				<div className={classes.right_div}>
					<div className={classes.add_suburb_div}>
						<InputBase
							className={classes.input}
							placeholder="Allocate Suburbs"
							value={name}
							onChange={(e) => setName(e.target.value)}
						/>
						<IconButton
							type="submit"
							className={classes.iconButton}
							aria-label="add"
							onClick={() => addLocation()}
						>
							<AddBoxOutlinedIcon />
						</IconButton>
					</div>
					<div>
						{box.map((item) => (
							<div key={item.id} className={classes.suburb_list_div}>
								<Typography variant="subtitle1">{item.value}</Typography>
								<IconButton
									type="submit"
									className={classes.iconButton}
									onClick={() => {
										setBox(box.filter((el) => el.id !== item.id));
									}}
								>
									<IndeterminateCheckBoxOutlinedIcon />
								</IconButton>
							</div>
						))}
					</div>
				</div>
			</div>
		</div>
	);
};

export default AddVolunteer;
