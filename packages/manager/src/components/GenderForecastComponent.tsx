
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import PieChartComponent from './pieChartComponentForGender'
const GenderForecastComponent = () => {
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '100%',
            height: '100%',
            // display: 'flex',
            // flexFlow: 'row wrap',
            // alignContent: 'flex-start'
        },
        overViewHeader: {
            marginLeft: 10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            // flex: '0 0 19%',
            fontWeight: 700,
        },
        childContainer: {
            marginTop: '20px',
            marginLeft: 'auto',
            marginRight: 'auto',
            height:'80%',
            width: '90%',
            display: 'flex',
            flexFlow: 'row wrap',
            // alignContent: 'flex-start'
        },
        child1: {
            flex: '0 0 25%'
        },
        child2: {
            flex: '0 0 25%',
            display: 'flex',
            flexFlow: 'column wrap',
        },
        label: {
            flex: '0 0 20%',
        },
        dot: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#A3A1FB',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        dot2: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#5FE3A1',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        dot3: {
            display: 'inline-block',
            height: '8px',
            backgroundColor: '#56D9FE',
            width: '8px',
            borderRadius: 5,
            marginBottom: '9px',
        },
        text: {
            marginTop: '10px',
            marginLeft: '10px',
            display: 'inline-block',
            color: '#232425',
            fontSize: 20,
        },
        labelText: {
            color: '#232425',
            opacity: 0.5,
            fontSize: 10,
            marginLeft: '20px',
        },
        line:{
            flex:'0 0 5%',
            marginRight:'190px',
            marginLeft: '20px',
        },
    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <Typography className={classes.overViewHeader}>
                Campaign Forecast by Gender
            </Typography>
            <div className={classes.childContainer}>
                <div className={classes.child1}>
                    <PieChartComponent title={'Total Positive Voters'} female={50} male={30} sum={100} unspecify={20} />
                </div>
                <div className={classes.child2}>
                    <div className={classes.label}>
                        <div className={classes.dot}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Female</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot2}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Intersex / Unspecified</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot3}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Male</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                </div>
                <div className={classes.child1}>
                    <PieChartComponent title={'Total Positive Voters'} female={50} male={30} sum={100} unspecify={20} />
                </div>
                <div className={classes.child2}>
                <div className={classes.label}>
                        <div className={classes.dot}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Female</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot2}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Intersex / Unspecified</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                    <div className={classes.label}>
                        <div className={classes.dot3}></div>
                        <Typography className={classes.text}>52.4%</Typography>
                        <Typography className={classes.labelText}>Male</Typography>
                    </div>
                    <div className={classes.line}>
                        <hr />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default GenderForecastComponent