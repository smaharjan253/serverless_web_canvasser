import React from 'react';
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Container, Button, Divider, Typography, Box } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import EnhancedTable from './sortedListComponent';
import OverViewComponent from './overViewComponent';
import ForcastChartComponent from './forcastChartComponent';
import SmallTableComponent from '@canvasser/components/src/smallTableComponent';
import TopVoterPreComponent from '@canvasser/components/src/topVoterPreComponent';
interface DashboardCommonComponentProps {
	editCallback: any;
	deleteCallback: (id: string) => void;
	data: any;
}
const DashboardCommonComponent: React.FC<DashboardCommonComponentProps> = (props) => {
	const { editCallback, deleteCallback, data } = props;
	const accounts = data.accounts;
	const records = data.records;
	const history = useHistory();

	const useStyles = makeStyles((theme) => ({
		wholePanel: {
			backgroundColor: '#F2F2F2',
			height: '100%',
			width: '100%'
		},
		tableContainer1: {
			width: '100%',
			height: '24%',
			paddingTop: theme.spacing(2),
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
			float: 'left'
			// backgroundColor: '#fff',
		},
		tableContainer4: {
			width: '100%',
			height: '29%',
			paddingTop: theme.spacing(2),
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
			float: 'left'
			// backgroundColor: '#fff',
		},
		tableContainer2: {
			width: '100%',
			paddingTop: theme.spacing(1),
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
			float: 'left'
			// backgroundColor: '#fff',
		},
		tableContainer3: {
			width: '100%',
			height: '47%',
			paddingTop: theme.spacing(1),
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
			float: 'left'
			// backgroundColor: '#fff',
		},
		tableContainer5: {
			width: '100%',
			height: '54%',
			paddingTop: theme.spacing(1),
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
			float: 'left'
			// backgroundColor: '#fff',
		},
		leftContainer: {
			width: '50%',
			height: '100%',
			float: 'left'
		},
		rightContainer: {
			width: '50%',
			height: '100%',
			float: 'right'
		}
	}));

	const processData1 = (accounts: any) => {
		return accounts.map((item: any) => {
			return [ item.firstname, item.suburbs.toString() ];
		});
	};

	const data1 = processData1(accounts);
	console.log({data1});
	const headCells1: any = [
		{ id: 0, numeric: false, disablePadding: true, label: 'USERNAME', needsort: true },
		{ id: 1, numeric: true, disablePadding: false, label: 'SURBURB', needsort: false },
		{ id: 2, numeric: true, disablePadding: false, label: 'ACTION', needsort: false }
	];

	const processData2 = (records: any) => {
		let results = new Map<string, any>();
		records.map((item: any) => {
			const add: number = item.successful > 0 ? 1 : 0;
			if (results.has(item.volunteerId)) {
				const volunteerId: string = item.volunteerId;
				results.set(volunteerId, {
					visits: results.get(volunteerId).visits + 1,
					positiveVisits: results.get(volunteerId).positiveVisits + add,
					name: item.volunteerName
				});
			} else {
				const volunteerId: string = item.volunteerId;
				results.set(volunteerId, {
					visits: 1,
					positiveVisits: add,
					name: item.volunteerName
				});
			}
		});
		// records.map(function (map: any, obj: any) {
		//     if (map.has(obj.volunteerId)) {
		//         map[obj.volunteerId] = {
		//             visits: map[obj.volunteerId].visits + 1,
		//             positiveVisits: map[obj.positiveVisits] + obj.ifPositive > 0 ? 1 : 0, name: obj.fullname
		//         }
		//     } else {
		//         map[obj.volunteerId] = { visits: 1, positiveVisits: obj.ifPositive > 0 ? 1 : 0, name: obj.fullname };
		//     }
		//     return map;
		// }, {});
		// console.log(results)
		const lastResult = Array.from(results, (x) => {
			return [ x[1].name, x[1].visits, x[1].positiveVisits ];
		});

		// console.log(lastResult)
		return lastResult;
		// return results.map((item:any)=>{
		//     return [item.fullname,item.visits,item.positiveVisits]
		// })
		// return []
	};

	// console.log(records)
	const data2 = processData2(records);
	// console.log(records)
	const headCells2: any = [
		{ id: 0, numeric: false, disablePadding: true, label: 'USERNAME', needsort: true },
		{ id: 1, numeric: true, disablePadding: false, label: 'TOTAL VISITS', needsort: false },
		{ id: 2, numeric: true, disablePadding: false, label: 'SUCCESSFUL VISITS', needsort: false }
	];

	const classes = useStyles();

	return (
		<Box className={classes.wholePanel}>
			<CssBaseline />
			<Container className={classes.leftContainer}>
				<Container className={classes.tableContainer1}>
					<OverViewComponent data={data} />
				</Container>
				<Container className={classes.tableContainer4}>
					<ForcastChartComponent data={data} />
				</Container>
				<Container className={classes.tableContainer2}>
					<SmallTableComponent
						editCallback={editCallback}
						deleteCallback={deleteCallback}
						pageSize={8}
						minH={500}
						actionShow={true}
						data={data1}
						headCell={headCells1}
						addOrMore={true}
						tableTitle="Manage User"
					/>
				</Container>
			</Container>
			<Container className={classes.rightContainer}>
				<Container className={classes.tableContainer3}>
					<SmallTableComponent
						editCallback={null}
						deleteCallback={() => null}
						pageSize={6}
						minH={800}
						actionShow={false}
						data={data2}
						headCell={headCells2}
						addOrMore={false}
						tableTitle="Volunteer Activity"
					/>
				</Container>
				<Container className={classes.tableContainer5}>
					<TopVoterPreComponent data={data} />
				</Container>
			</Container>
		</Box>
	);
};

export default DashboardCommonComponent;
