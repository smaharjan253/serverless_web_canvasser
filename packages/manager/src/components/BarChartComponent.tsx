import React, { PureComponent } from 'react';
import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
    {
        name: 'Page A', uv: 4000, pv: 0.1, amt: 2400,
    },
    {
        name: 'Page B', uv: 3000, pv: 0.1, amt: 2210,
    },
    {
        name: 'Page C', uv: 2000, pv: 0.2, amt: 2290,
    },
    {
        name: 'Page D', uv: 2780, pv: 0.2, amt: 2000,
    },
    {
        name: 'Page E', uv: 1890, pv: 0.3, amt: 2181,
    },
    {
        name: 'Page F', uv: 2390, pv: 0.1, amt: 2500,
    },
    {
        name: 'Page G', uv: 3490, pv: 0, amt: 2100,
    },
];
const toPercent = (decimal: any, fixed = 0) => `${(decimal * 100).toFixed(fixed)}%`;
interface BarComponentProps {
    color:string,
}
export default class BarChartComponent extends PureComponent<BarComponentProps> {
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/30763kr7/';

    render() {
        return (
            <BarChart
                width={550}
                height={230}
                data={data}
                margin={{
                    top: 5, right: 0, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="1000 100" vertical={false} />
                <XAxis dataKey="name" />
                <YAxis tickFormatter={toPercent} axisLine={false} tickLine={false} />
                <Tooltip />
                {/* <Legend /> */}
                <Bar dataKey="pv" fill={this.props.color} barSize={20} />
            </BarChart>
        );
    }
}
