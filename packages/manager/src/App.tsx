import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import NavBar from './components/NavBar';
import Dashboard from './components/Dashboard';
import Analytics from './components/Analytics';
import Questions from './components/Questions';
import Volunteers from './components/Volunteers';
import Help from './components/Help';
import AddVolunteer from './components/AddVolunteer';
import AddQuestions from './components/AddQuestions';
import Preference from './components/Preference';
import VolunteerActivity from './components/VolunteerActivity';
import DefaultQuestions from './components/DefaultQuestions';
import Amplify from 'aws-amplify';
import awsmobile from '@canvasser/components/src/aws-exports';

Amplify.configure(awsmobile);
const App = () => {
	return (
		<BrowserRouter>
			<div>
				<NavBar />
				<Route exact path="/" component={Dashboard}/>
				<Route path="/Volunteers" component={Volunteers} />
				<Route path="/Questions" component={Questions} />
				<Route path="/Analytics" component={Analytics} />
				<Route path="/Help" component={Help} />
				<Route path="/Preference" component={Preference} />
				<Route path='/AddVolunteer' component={AddVolunteer}/>
				{/* <Route path="/" component={AddVolunteer} /> */}
				<Route path="/AddQuestions" component={AddQuestions} />
				<Route path="/VolunteerActivity" component={VolunteerActivity} />
				<Route path="/DefaultQuestions" component={DefaultQuestions} />
			</div>
		</BrowserRouter>
	);
};

export default App;
