import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import Dashboard from './components/Dashboard'
import Analytics from './components/Analytics'
import Locations from './components/Locations'
import Managers from './components/Managers'
import Help from './components/Help'
import AddManager from './components/AddManager'
import AddLocation from './components/AddLocation'
import ManagerActivity from './components/ManagerActivity'

const App = () => {
    return(
        <BrowserRouter>
            <div>
                <NavBar />
                <Route exact path="/" component={Dashboard}/>
                <Route path="/Managers" component={Managers}/>
                <Route path="/Locations" component={Locations}/>
                <Route path="/Analytics" component={Analytics}/>
                <Route path="/Help" component={Help}/>
                <Route path='/AddManager' component={AddManager}/>
                <Route path='/AddLocation' component={AddLocation}/>
                <Route path='/ManagerActivity' component={ManagerActivity}/>
            </div>
        </BrowserRouter>
    )
}

export default App