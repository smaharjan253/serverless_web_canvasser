import React from 'react'
import { Typography } from "@material-ui/core"
import AnalyticComponent from './AnalyticComponent'

const Analytics = () => {
    return(
        <div className="container">
            <Typography className="page-heading" variant="h5">  
                Create Manager Account
            </Typography>
            <br/>
            <AnalyticComponent />
        </div>
    )
}

export default Analytics