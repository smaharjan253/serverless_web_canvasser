import React, { useState } from 'react';
import { Button, Typography, MenuItem, FormControl, InputLabel,
Select, Grid} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
//import addLocationProps from './component-styles/AddLocationData';
import useStyles from './component-styles/NavBarStyle';
import AddIcon from '@material-ui/icons/Add';

interface AddLocationProps {
    box1: any;
}

const AddLocation: React.FC<AddLocationProps>  = () => {
    const classes = useStyles();
    const [country, setCountry] = useState('');
    const [state, setState] = useState('');
    const [suburb, setSuburb] = useState('');
    const [openCountry, setOpenCountry] = useState(false);
    const [openState, setOpenState] = useState(false);
    const [openSuburb, setOpenSuburb] = useState(false);
    
    const handleChangeCountry = (event:any) => {
        setCountry(event.target.value);
    };

    const handleChangeState = (event:any) => {
        setState(event.target.value);
    };

    const handleChangeSuburb = (event:any) => {
        setSuburb(event.target.value);
    };
    
    const handleCloseCountry = () => {
        setOpenCountry(false);
    };

    const handleOpenCountry = () => {
        setOpenCountry(true);
        console.log('open3')
    };

    const handleCloseState = () => {
        setOpenState(false);
    };

    const handleOpenState = () => {
        setOpenState(true);
        console.log('open2')
    };

    const handleCloseSuburb = () => {
        setOpenSuburb(false);
    };
    
    const handleOpenSuburb = () => {
        console.log('open1')
        setOpenSuburb(true);
    };
        
    return(
        <div className="container">
            <div className="mainContainer">
                <Typography className="page-heading" variant="h5">  
                    Add Location
                </Typography>
                <br/>
                
                    <form >
                        <Grid className= "addLocationForm" container spacing={5}>
                            <Grid item className="addLocationCountry" xs={4}>
                                <Typography variant="subtitle1" style={{marginTop: '22px'}}>
                                    Country:
                                </Typography>
                                <FormControl className={classes.formControl}>
                                    <InputLabel variant='outlined'>
                                        Select Country
                                    </InputLabel>
                                    
                                    <Select
                                        id="open-select-country"
                                        variant="outlined"
                                        open={openCountry}
                                        onClose={handleCloseCountry}
                                        onOpen={handleOpenCountry}
                                        value={country}
                                        onChange={handleChangeCountry}
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={'australia'}>Australia</MenuItem>
                                        <MenuItem value={'nepal'}>Nepal</MenuItem>
                                        <MenuItem value={'china'}>China</MenuItem>
                                        <MenuItem value={'vietnam'}>Vietnam</MenuItem>
                                    </Select>
                                    <br/><br/>
                                    <TextField
                                        id="textfield-location"
                                        label='Enter Country'
                                        variant="outlined"
                                    />
                                </FormControl>
                                
                            </Grid>

                            <Grid item className="addLocationState" xs={4}>
                                <Typography variant="subtitle1" style={{marginTop: '22px'}}>
                                    State:
                                </Typography>
                                <FormControl className={classes.formControl}>
                                    <InputLabel variant='outlined'>
                                        Select State
                                    </InputLabel>
                                    <Select
                                        id="open-select-state"
                                        variant="outlined"
                                        open={openState}
                                        onClose={handleCloseState}
                                        onOpen={handleOpenState}
                                        value={state}
                                        onChange={handleChangeState}
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={'new-south-wales'}>New South Wales</MenuItem>
                                        <MenuItem value={'victoria'}>Victoria</MenuItem>
                                        <MenuItem value={'queensland'}>Queensland</MenuItem>
                                    </Select>
                                    <br/><br/>
                                    <TextField
                                        id="textfield-location"
                                        label='Enter State'
                                        variant="outlined"
                                    />
                                </FormControl>
                            </Grid>
                                
                            <Grid item className="addLocationSuburb" xs={4}>
                                <Typography variant="subtitle1" style={{marginTop: '22px'}}>
                                    Suburb:
                                </Typography>
                                <FormControl className={classes.formControl}>
                                    <InputLabel variant='outlined'>
                                        Select Suburb
                                    </InputLabel>
                                    <Select
                                        id="open-select-suburb"
                                        variant="outlined"
                                        open={openSuburb}
                                        onClose={handleCloseSuburb}
                                        onOpen={handleOpenSuburb}
                                        value={suburb}
                                        onChange={handleChangeSuburb}
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={'newtown'}>Newtown</MenuItem>
                                        <MenuItem value={'central'}>Central</MenuItem>
                                        <MenuItem value={'ashfield'}>Ashfield</MenuItem>
                                        <MenuItem value={'riverwood'}>Riverwood</MenuItem>
                                    </Select>
                                    <br/><br/>
                                    <TextField
                                        id="textfield-location"
                                        label='Enter Suburb'
                                        variant="outlined"
                                    />
                                    <Button 
                                        // onClick={() => addSuburb()}
                                    >
                                        <AddIcon/>
                                    </Button>
                                </FormControl>
                            </Grid>
                        </Grid>

                        <br/> <br/> <br/>
                        <div className="submitButtonDiv">
                            
                                <Button className="submitButton" variant="contained" color="primary" disableElevation>
                                    Submit
                                </Button>
                            
                        </div>
                        
                    </form>
            </div>
        </div>
    )
}

export default AddLocation