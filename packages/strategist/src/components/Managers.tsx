import React from 'react'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import useStyles from './component-styles/NavBarStyle';
import { Box } from '@material-ui/core';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';

const Managers = () => {
    const classes = useStyles();

    return(
        <div className="container">
            <div className="mainContainer">
                <Box className={classes.inComponentButton}>
                    <Button 
                        component={Link} 
                        to="/AddManager"
                    >
                        Add User
                        <AddBoxOutlinedIcon />
                    </Button>
                </Box>
                <h1 style={{marginLeft:"150px", marginTop:"50px"}}>This is Managers</h1>
            </div>
        </div>
    )
}

export default Managers