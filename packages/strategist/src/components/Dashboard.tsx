import React from 'react'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from './component-styles/NavBarStyle'


const Dashboard = () => {
    const classes = useStyles();
    
    return(
        <div className="container">
            <div className="mainContainer">
                <div className={classes.inComponentButton}>
                    <Button 
                        component={Link} 
                        to="/ManagerActivity"
                        variant="outlined"
                        color="primary"
                    >
                        Manager Activity
                    </Button>
                </div>
                <h1 style={{marginLeft:"150px", marginTop:"50px"}}>This is Dashboard</h1>
                
            </div>
        </div>
    )
}

export default Dashboard