import React from 'react'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from './component-styles/NavBarStyle'
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';

const Locations = () => {
    const classes = useStyles();

    return(
        <div className="container">
            <div className="mainContainer">
                <div className={classes.inComponentButton}>
                    <Button 
                        component={Link} 
                        to="/AddLocation"
                    >
                        Add Location
                        <AddBoxOutlinedIcon />
                    </Button>
                </div>
                <h1 style={{marginLeft:"150px", marginTop:"50px"}}>This is Locations</h1>
            </div>
        </div>
    )
}
//#0A437C
export default Locations