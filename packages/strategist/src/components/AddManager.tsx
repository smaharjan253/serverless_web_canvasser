import React, { useState } from 'react';
import { makeStyles, Button, Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import dot from 'dot-object'
import { useForm } from '@canvasser/components/src/useForm';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles(theme => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '50%',
        height: 60
      },
    },
    root2: {
        '& > *': {
          textAlign: 'center',
          width: "44.5%"
        },
      },
}));

interface addManagerProps {
    box1: any;
}

const AddManager: React.FC<addManagerProps>  = () => {
    const classes = useStyles();
    const [ box, setBox ] = useState([{value: null}])
    const { values, useInput, isValid } = useForm({
		email: '',
		password: ''
    });
    
    function addLocation(){
        // setBox([...box, ""])
        const values = [...box];
        if(box.length<3){
            values.push({value:null})
            setBox(values)
        } 
    }

    function removeLocation(index:any){
        const values = [...box];
        values.splice(index, 1);
        setBox(values);
    }

    function handleChange(index:any, e:any){
        const values = [...box]
        box[index].value = e.target.value
        setBox(values)
    }

    return(
        <div className="container">
            <div className="mainContainer">
                <Typography className="page-heading" variant="h5">  
                    Create Manager Account
                </Typography>
                <br/>

                <form className={classes.root} noValidate autoComplete="off">
                    <div className="addManagerLocationInput">
                        <TextField
                            style={{width:"20vw"}}
                            className="textfields"
                            id="Preferred Location"
                            label="Preferred Location"
                            variant='outlined'
                        />
                        <Button 
                            onClick={() => addLocation()}>
                            <AddIcon/>
                        </Button>
                        
                        <br/>
                        <br/>
                        {box.map((location, index) => 
                            <div key={`${location}-${index}`} className={classes.root2}>
                                <TextField 
                                    value={location.value} 
                                    variant="outlined"
                                    label="More Location"
                                    onChange={e => handleChange(index, e)}
                                />
                                <Button 
                                    onClick={() => removeLocation(index)}
                                    style={{width: "20px"}}
                                >
                                    <RemoveIcon/>
                                </Button>
                                <br/><br/>
                            </div>
                        )}
                    
                    </div>
                    
                    <div className={classes.root}>
                        <TextField
                            className="textfields"
                            label="Full Name"
                            variant="outlined"
                        />
                        <br/>

                        <TextField
                            className="textfields"
                            label="Date of Birth"
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            className="textfields"
                            label="Contact No."
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            className="textfields"
                            label="E-mail"
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            className="textfields"
                            label="Password"
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            className="textfields"
                            label="Confirm Password"
                            variant="outlined"
                        />
                        <br/>
                    </div>
                    <div className={classes.root}>
                        <Button variant="contained" color="primary" style={{top:"400px"}}>
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default AddManager