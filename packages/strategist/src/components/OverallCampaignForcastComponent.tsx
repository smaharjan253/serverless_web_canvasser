
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import PieChartComponent from './pieChartComponent'

function toPercent2(point: any) {
    var str = Number(point * 100).toFixed(2);
    str += "%";
    return str;
}
interface OverViewComponentProps {
    positiveVisits: number,
    negativeVisits: number,
    sum: number,
}
const OverallCampaignForcastComponent: React.FC<OverViewComponentProps> = (props) => {
    const { positiveVisits,negativeVisits,sum } = props

    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '100%',
            height: '100%',
            display: 'flex',
            flexFlow: 'column wrap',
        },
        overViewHeader: {
            
            marginLeft:10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            flex: '0 0 19%',
            fontWeight:700,
        },
        piechart: {
            flex: '0 0 22%',
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        line: {
            flex: '0 0 5%',
            marginLeft: 40,
            marginRight: 40
        },
        labels: {
            flex: '0 0 27%',
            width: '80%',
            marginLeft: 'auto',
            marginRight: 'auto',
            display: 'flex',
            flexFlow: 'row wrap',
        },
        child1: {
            flex: '0 0 45%',
            display: 'flex',
            flexFlow: 'row wrap',
        },
        child2: {
            flex: '0 0 10%',
            borderLeft:'solid #ACC0D8 1px',
            marginBottom:19,
        },
        dot: {
            marginTop: 10,
            flex: '0 0 8px',
            height: '8px',
            backgroundColor: '#FF5660',
            width: '8px',
            borderRadius: 5,
        },
        dot2: {
            marginTop: 10,
            flex: '0 0 8px',
            height: '8px',
            backgroundColor: '#F0F2F8',
            width: '8px',
            borderRadius: 5,
        },
        text: {
            marginLeft:15,
            flex: '0 0 90%',
        },
        percentText: {
            color: '#232425',
            fontSize: 25,
            marginLeft: 8,
            flex: '0 0 80%',
            height:20,
        },
        textText:{
            color: '#232425',
            opacity:0.5,
            fontSize:10,
        }

    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <Typography className={classes.overViewHeader}>
                Overall Campaign Forecast
            </Typography>
            <div className={classes.piechart}>
                <PieChartComponent positiveVisits={positiveVisits} negativeVisits={negativeVisits} sum={sum} />
            </div>
            <div className={classes.line}>
                <hr />
            </div>
            <div className={classes.labels}>
                <div className={classes.child1}>
                    <div className={classes.dot}></div>
                        <Typography className={classes.percentText}>
                            {toPercent2(positiveVisits / sum)}
                        </Typography>
                    
                    <div className={classes.text}>
                        <Typography className={classes.textText}>
                            Positive Voters
                        </Typography>
                    </div>
                </div>
                <div className={classes.child2}>

                </div>
                <div className={classes.child1}>
                <div className={classes.dot2}></div>
                        <Typography className={classes.percentText}>
                            {toPercent2(negativeVisits / sum)}
                        </Typography>
                    
                    <div className={classes.text}>
                        <Typography className={classes.textText}>
                            Negative Voters
                        </Typography>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OverallCampaignForcastComponent