const buttonProps = [
    {
      id: 1,
      name: 'dashboard',
      path: '/'
    },
    {
      id: 2,
      name: 'managers',
      path: '/managers'
    },
    {
      id: 3,
      name: 'locations',
      path: '/locations'
    },
    {
      id: 4,
      name: 'analytics',
      path: '/analytics'
    },
  ]

  export default buttonProps