const analyticsData = 
// [{name:"First",
//                         data:[
//                             {name: 'Jan', value: 100}, 
//                             {name: 'Feb', value: 200},
//                             {name: 'Mar', value: 100},
//                             ],  
//                         },
//                         {name:"Second",
//                         data:[  
//                             {name: 'Apr', value: 400},
//                             {name: 'May', value: 105},
//                             {name: 'Jun', value: 12.5},
//                             ],  
//                         },
//                         {name:"Third",
//                         data:[
//                             {name: 'Jul', value: 100}, 
//                             {name: 'Aug', value: 200},
//                             {name: 'Sep', value: 100},
//                             ], 
//                         }, 
//                         {name:"Fourth",
//                         data:[
//                             {name: 'Oct', value: 400},
//                             {name: 'Nov', value: 105},
//                             {name: 'Dec', value: 12.5},
//                             ]
//                         }
//                         ]
                    
                    
                [
                            {name: 'Jan', postiveVotes: 100, negativeVotes: 100, amt: 300}, 
                            {name: 'Feb', postiveVotes: 200, negativeVotes: 400, amt: 350},
                            {name: 'Mar', postiveVotes: 100, negativeVotes: 200, amt: 200},
                             
                            {name: 'Apr', postiveVotes: 400, negativeVotes: 100, amt: 250},
                            {name: 'May', postiveVotes: 105, negativeVotes: 50, amt: 100},
                            {name: 'Jun', postiveVotes: 12.5, negativeVotes: 150, amt: 90},
                            
                            {name: 'Jul', postiveVotes: 100, negativeVotes: 100, amt: 300}, 
                            {name: 'Aug', postiveVotes: 200, negativeVotes: 400, amt: 350},
                            {name: 'Sep', postiveVotes: 100, negativeVotes: 200, amt: 200},
                            
                            {name: 'Oct', postiveVotes: 400, negativeVotes: 100, amt: 250},
                            {name: 'Nov', postiveVotes: 105, negativeVotes: 50, amt: 100},
                            {name: 'Dec', postiveVotes: 12.5, negativeVotes: 150, amt: 90},
                            
                    ]

export default analyticsData