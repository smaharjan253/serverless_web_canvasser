const addLocationProps = [
    {
        id: "country",
        label: 'Country'
    },
    {
        id: "state",
        label: 'State'
    },
    {
        id: "suburb",
        label: 'Suburb'
    }
]

export default addLocationProps