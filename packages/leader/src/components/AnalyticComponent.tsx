
import { makeStyles } from '@material-ui/core/styles';
import React from 'react'
import { useState } from 'react'
import { CssBaseline, Container, Button, Divider, Typography, Box } from "@material-ui/core"
import OverallCampaignForcastComponent from '@canvasser/components/src/OverallCampaignForcastComponent'
import EmploymentComponent from '@canvasser/components/src/EmploymentComponent'
import GenderForecastComponent from '@canvasser/components/src/GenderForecastComponent'
import BarChartComponent from '@canvasser/components/src/BarChartComponent'
import LineGraphComponent from '@canvasser/components/src/LineGraphComponent'
import jsonData from "@canvasser/components/src/stack.json"

const extractSuccessful = (data:any)=>{
    return data.filter((item:any)=>{
        if(item.successful === 1){
            return 1
        }else{
            return 0
        }
    })
}
function toPercent2(point: any) {
    var str = Number(point * 100).toFixed(2);
    str += "%";
    return str;
}
const processDataGender = (records: any) => {
    const sum = records.length
    let results = new Map<string, any>();
    records.map((item: any) => {
        if (results.has(item.employment)) {
            results.set(item.employment, {
                num: results.get(item.employment).num + 1,
                name:item.employment
            })
        } else {
            const employment: string = item.employment;
            results.set(employment, {
                num:1,
                name:item.employment
            })
        }
        
    });
    const lastResult= Array.from(results,x=>{
        return {title:x[1].name,percent:toPercent2(x[1].num/sum)}
    })

    // console.log(lastResult)
    return lastResult
}
const AnalyticComponent = () => {
    const [data, setData] = useState({records:[],state:false});
    
    const requestData = ()=>{
        const url = jsonData.ServiceEndpoint + '/getRecordsBySurburbs'
        console.log(url)
        // const url = ''
        const callback = (dataLoad:any)=>{
            setData({records:dataLoad,state:true})
        }
        const surburbs = ['surburb1','surburb2']
        fetch(url, {
            method: 'POST',
            headers: { 'Accept': 'application/json',
            'x-api-key': jsonData.apikey,
           },
            body: JSON.stringify({
                surburbs: surburbs,
            }),
        })
            .then(function (response) {
                return response.json()
            })
            .then(function (myjson) {
                console.log(myjson);
                
                callback(myjson)
                
            })
            .catch(error => console.error('Error:', error));;
    }
    if(!data.state){
        requestData()
    }
    const successfulData = extractSuccessful(data.records)
    const positiveVisitsData = successfulData.filter((item:any)=>{
        return item.ifPositive>0?1:0 
    })
    const positiveVisits = positiveVisitsData.length
    const negativeVisits = successfulData.length - positiveVisits
    const employmentData = processDataGender(successfulData)
    const ageGroups = ['18-24','25-34','35-44','45-54','55-64','65+']

    const ageDataForAll = ageGroups.map((item)=>{
        const sum = successfulData.length
        const num = successfulData.filter((item1:any)=>{
            return item1.ageGroup === item?1:0
        }).length
        return {name:item,count:sum===0?0:num/sum}
    })

    const ageDataForPositive = ageGroups.map((item)=>{
        const sum = positiveVisitsData.length
        const num = positiveVisitsData.filter((item1:any)=>{
            return item1.ageGroup === item?1:0
        }).length
        return {name:item,count:sum===0?0:num/sum}
    })

    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    const monthData = months.map((item)=>{
        const monthAllData = successfulData.filter((item1:any)=>{
            return item1.month === item?1:0
        })
        const monthPositive = monthAllData.filter((item:any)=>{
            return item.ifPositive>0?1:0
        }).length
        const monthNegative = monthAllData.length - monthPositive
        return {name:item,postiveVotes:monthPositive,negativeVotes:monthNegative}
    })
    // console.log(employmentData)
    const useStyles = makeStyles(theme => ({
        wholePanel: {
            width: '1300px',
            height: '700px',
            display: 'flex',
            flexFlow: 'row wrap',
            alignContent: 'flex-start'
        },
        child1: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 47%',
            height: '40%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child2: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 47%',
            height: '40%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child3: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 5%',
        },
        child4: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 99%',
            height: '43%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        child5: {
            marginTop: '10px',
            marginBotton: '10px',
            flex: '0 0 99%',
            height: '48%',
            border: 'solid #DDEEFA 1px',
            borderRadius: '8px',
            backgroundColor:'#FFFFFF',
        },
        overViewHeader: {
            marginLeft: 10,
            fontSize: 20,
            fontFamily: 'Segoe UI',
            color: '#000000',
            fontWeight: 700,
        },
        barchartP: {
            marginTop: '20px'
        }
    }));
    const classes = useStyles();
    return (
        <div className={classes.wholePanel}>
            <div className={classes.child5}>
                <LineGraphComponent data={monthData}/>
            </div>
            <div className={classes.child1}>
                <OverallCampaignForcastComponent positiveVisits={positiveVisits} negativeVisits={negativeVisits} sum={successfulData.length}/>
            </div>
            <div className={classes.child3}>
            </div>
            <div className={classes.child2}>
                <EmploymentComponent data={employmentData}/>
            </div>
            <div className={classes.child4}>
                <GenderForecastComponent data={successfulData}/>
            </div>
            <div className={classes.child1}>
                <Typography className={classes.overViewHeader}>
                    Overall Voters’ Demographics
                </Typography>
                <div className={classes.barchartP}>
                    <BarChartComponent data={ageDataForAll} color={'#4880FF'}/>
                </div>
            </div>
            <div className={classes.child3}>
            </div>
            <div className={classes.child2}>
                <Typography className={classes.overViewHeader}>
                    Positive Voters’ Demographics
                </Typography>
                <div className={classes.barchartP}>
                    <BarChartComponent data={ageDataForPositive} color={'#FFBE05'}/>
                </div>
            </div>
        </div>
    )
}

export default AnalyticComponent