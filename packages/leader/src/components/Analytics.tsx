import React from 'react'
import { Typography } from "@material-ui/core"
import AnalyticComponent from './AnalyticComponent'

const Analytics = () => {
    return(
        <div className="container">
           
            <AnalyticComponent />
        </div>
    )
}

export default Analytics