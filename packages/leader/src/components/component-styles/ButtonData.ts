const buttonProps = [
    {
      id: 1,
      name: 'dashboard',
      path: '/'
    },
    {
      id: 2,
      name: 'analytics',
      path: '/analytics'
    },
    {
      id: 3,
      name: 'preference',
      path: '/preference'
    },
  ]

  export default buttonProps