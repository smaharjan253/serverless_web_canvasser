import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import Dashboard from './components/Dashboard'
import Analytics from './components/Analytics'
import Help from './components/Help'
import Preference from './components/Preference'

const App = () => {
    return(
        <BrowserRouter>
            <div>
                <NavBar />
                <Route exact path="/" component={Dashboard}/>
                <Route path="/Preference" component={Preference}/>
                <Route path="/Analytics" component={Analytics}/>
                <Route path="/Help" component={Help}/>
            </div>
        </BrowserRouter>
    )
}

export default App